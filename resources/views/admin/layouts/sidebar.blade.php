<li class="nav-item @if(url()->current() == url('admin')) active @endif ">
    <a href="{{url('admin')}}" class="nav-link">
        <i class="material-icons">dashboard</i>
        <p>{{__('admin.sidebar.home')}}</p>
    </a>
</li>
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_managements" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_managements')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_managements'))===0) in @endif" id="app_managements" @if(strpos(url()->current() , url('admin/app_managements'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/admins'))===0) active @endif">
                <a href="{{url('admin/app_managements/admins')}}" class="nav-link">
                    <i class="material-icons">group</i>
                    <p>{{__('admin.sidebar.admins')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_settings" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_settings')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_settings'))===0) in @endif" id="app_settings" @if(strpos(url()->current() , url('admin/app_settings'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/countries'))===0) active @endif">
                <a href="{{url('admin/app_settings/countries')}}" class="nav-link">
                    <i class="material-icons">language</i>
                    <p>{{__('admin.sidebar.countries')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/cities'))===0) active @endif">
                <a href="{{url('admin/app_settings/cities')}}" class="nav-link">
                    <i class="material-icons">location_city</i>
                    <p>{{__('admin.sidebar.cities')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/categories'))===0) active @endif">
                <a href="{{url('admin/app_settings/categories')}}" class="nav-link">
                    <i class="material-icons">label</i>
                    <p>{{__('admin.sidebar.categories')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/features'))===0) active @endif">
                <a href="{{url('admin/app_settings/features')}}" class="nav-link">
                    <i class="material-icons">sort</i>
                    <p>{{__('admin.sidebar.features')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/utilities'))===0) active @endif">
                <a href="{{url('admin/app_settings/utilities')}}" class="nav-link">
                    <i class="material-icons">category</i>
                    <p>{{__('admin.sidebar.utilities')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/contacts'))===0) active @endif">
                <a href="{{url('admin/app_settings/contacts')}}" class="nav-link">
                    <i class="material-icons">perm_phone_msg</i>
                    <p>{{__('admin.sidebar.contacts')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/settings'))===0) active @endif">
                <a href="{{url('admin/app_settings/settings')}}" class="nav-link">
                    <i class="material-icons">settings</i>
                    <p>{{__('admin.sidebar.settings')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>

<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_users" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_users')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_users'))===0) in @endif" id="app_users" @if(strpos(url()->current() , url('admin/app_users'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_users/users'))===0) active @endif">
                <a href="{{url('admin/app_users/users')}}" class="nav-link">
                    <i class="material-icons">group</i>
                    <p>{{__('admin.sidebar.users')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_users/providers'))===0) active @endif">
                <a href="{{url('admin/app_users/providers')}}" class="nav-link">
                    <i class="material-icons">person</i>
                    <p>{{__('admin.sidebar.providers')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>

<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_data" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_data')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_data'))===0) in @endif" id="app_data" @if(strpos(url()->current() , url('admin/app_data'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/places'))===0) active @endif">
                <a href="{{url('admin/app_data/places')}}" class="nav-link">
                    <i class="material-icons">account_balance</i>
                    <p>{{__('admin.sidebar.places')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/orders'))===0) active @endif">
                <a href="{{url('admin/app_data/orders')}}" class="nav-link">
                    <i class="material-icons">lists</i>
                    <p>{{__('admin.sidebar.orders')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
