@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6" onclick="window.location='{{url('admin/app_users/users')}}'" style="cursor: pointer">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">people</i>
                </div>
                <div class="card-content">
                    <p class="category">{{__('admin.sidebar.users')}}</p>
                    <h3 class="title">{{\App\Models\User::where('type',\App\Models\User::TYPE['Customer'])->count()}}</h3>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6" onclick="window.location='{{url('admin/app_users/providers')}}'" style="cursor: pointer">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">
                    <p class="category">{{__('admin.sidebar.providers')}}</p>
                    <h3 class="title">{{\App\Models\User::where('type',\App\Models\User::TYPE['Provider'])->count()}}</h3>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats" onclick="window.location='{{url('admin/app_data/places')}}'" style="cursor: pointer">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">account_balance</i>
                </div>
                <div class="card-content">
                    <p class="category">{{__('admin.sidebar.places')}}</p>
                    <h3 class="title">{{\App\Models\Place::count()}}</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">  {{__('admin.Home.n_send_general')}} </h4>
                </div>
                <div class="card-content">
                    <form action="{{url('admin/notification/send')}}" method="post">
                        @csrf
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12 btn-group required">
                                    <label for="title">{{__('admin.Home.n_title')}} :</label>
                                    <input type="text" required="" name="title" id="title" class="form-control" placeholder="{{__('admin.Home.n_enter_title')}}">
                                </div>
                                <div class="col-md-12 btn-group required">
                                    <label for="msg">{{__('admin.Home.n_text')}} :</label>
                                    <input type="text" required="" name="msg" id="msg" class="form-control" placeholder="{{__('admin.Home.n_enter_text')}}">
                                </div>
                                <div class="col-md-4 required">
                                    <label for="city">{{__('Models/City.the_city')}} :</label>
                                    <select name="city" id="city" class="form-control">
                                        <option value="">{{__('-')}}</option>
                                        @foreach(\App\Models\City::all() as $City)
                                            <option value="{{$City->id}}">{{$City->final_name}}</option>
                                        @endforeach
                                     </select>
                                </div>
                                <div class="col-md-4 required">
                                    <label for="category">{{__('Models/Category.the_category')}} :</label>
                                    <select name="category" id="category" class="form-control">
                                        <option value="">{{__('-')}}</option>
                                        @foreach(\App\Models\Category::all() as $Category)
                                            <option value="{{$Category->id}}">{{$Category->final_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4  required">
                                    <label for="type">{{__('admin.Home.n_type')}} :</label>
                                    <select required name="type" id="type" class="form-control">
                                        <option value="0">{{__('admin.Home.n_type_0')}}</option>
                                        <option value="1">{{__('admin.Home.n_type_1')}}</option>
                                        <option value="2">{{__('admin.Home.n_type_2')}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" id="send" name="submit" class="btn btn-primary btn-block">{{__('admin.Home.n_send')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        let category = $('#category');
        category.change(function () {
            if(category.val() !==''){
                let type = $('#type');
                type.val('2');
            }
        });

    </script>
@endsection
