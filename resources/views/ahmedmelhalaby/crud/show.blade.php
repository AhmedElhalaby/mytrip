@extends('ahmedmelhalaby.crud.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__(($TheName))}}</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        @foreach($Fields as $Field)
                            @if(isset($Field['editable']))
                                @if($Field['editable'])
                                    {!! \App\Master::Shows($Field,$Object->{$Field['name']}) !!}
                                @endif
                            @else
                            {!! \App\Master::Shows($Field,$Object->{$Field['name']}) !!}
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
