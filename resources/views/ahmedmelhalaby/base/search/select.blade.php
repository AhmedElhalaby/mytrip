<div class="form-group" style="margin:0;padding: 0 ">
    <label for="{{$Column['name']}}" class="hidden"></label>
    <select name="{{$Column['name']}}" style="margin: 0;padding: 0" id="{{$Column['name']}}" class="form-control">
        <option value="">-</option>
        @foreach($Column['data'] as $key => $text)
            <option value="{{$key}}" @if(app('request')->input($Column['name']) == $key) selected @endif>{{$text}}</option>
        @endforeach
    </select>
</div>
