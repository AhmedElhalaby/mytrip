<span class="text-primary">
    @if(isset($column['relation']['url']))
        <a href="{{url($column['relation']['url'].'/'.$object->{$column['relation']['entity'] }->id.'/edit')}}" target="_blank">
    @endif
        {{ @$object->{$column['relation']['entity'] }->{$column['relation']['name']} }}
    @if(isset($column['relation']['url']))
        </a>
    @endif

</span>
