<div class="col-md-12">
    <label for="{{$Field['name']}}" class="control-label">{{__($Field['lng_name'])}} @if($Field['is_required'])*@endif</label>
    <input type="file" multiple name="{{$Field['name']}}[]" id="{{$Field['name']}}"  class="">
    @if ($errors->has($Field['name']))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first($Field['name']) }}</strong>
        </span>
    @endif
</div>
