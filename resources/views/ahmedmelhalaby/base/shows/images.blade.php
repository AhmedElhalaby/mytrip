<div class="col-md-12">
    @if(isset($value))
        @foreach($value as $img)
            <img src="{{asset($img->attachment)}}" style="width: 120px;height: 120px;margin-right: 5px;margin-left: 5px;display: inline-block" alt="upload image" class="thumbnail"/>
        @endforeach
    @endif
</div>
