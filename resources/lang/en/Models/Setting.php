<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'settings' => 'Settings',
    'setting' => 'Setting',
    'the_setting' => 'The Setting',
    'key' => 'Key',
    'name' => 'Name En',
    'name_ar' => 'Name Ar',
    'value' => 'Content',
    'value_ar' => 'Content Ar',
];
