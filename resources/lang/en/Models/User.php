<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'users' => 'Customers',
    'user' => 'Customer',
    'the_user' => 'The Customer',

    'name' => 'Name',
    'email' => 'E-Mail',
    'mobile' => 'Mobile',
    'image' => 'Image',
    'city_id' => 'City',
    'gender' => 'Gender',
    'dob' => 'Date Of Birth',
    'address' => 'Address',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'Gender'=>[
        'male'=>'Male',
        'female'=>'Female',
    ],
    'is_active' => 'Status',

];
