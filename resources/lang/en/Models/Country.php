<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'countries' => 'Countries',
    'country' => 'Country',
    'the_country' => 'The Country',
    'name' => 'Name En',
    'name_ar' => 'Name Ar',
    'country_code' => 'Code',
    'is_active' => 'Status',
];
