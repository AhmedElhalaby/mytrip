<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'features' => 'Features',
    'feature' => 'Feature',
    'the_feature' => 'The Feature',
    'name' => 'Name En',
    'name_ar' => 'Name Ar',
    'description' => 'Description',
    'description_ar' => 'Description Ar',
    'category_id' => 'Category',
    'image' => 'Image',
    'is_active' => 'Status',
];
