<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'cities' => 'Cities',
    'city' => 'City',
    'the_city' => 'The City',
    'name' => 'Name En',
    'name_ar' => 'Name Ar',
    'country_id' => 'Country',
    'area' => 'Area',
    'Areas' => [
        'E'=>'East',
        'W'=>'West',
        'S'=>'South',
        'N'=>'North',
        'M'=>'Middle',
    ],
    'is_active' => 'Status',
];
