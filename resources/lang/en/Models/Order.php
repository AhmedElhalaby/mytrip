<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'orders' => 'Reservations',
    'order' => 'Reservation',
    'the_order' => 'The Reservation',
    'user_id' => 'User',
    'place_id' => 'Place',
    'status' => 'Status',
    'enter_at' => ' From',
    'leave_at' => 'To',
    'Statuses' => [
        'new'=>'New',
        'accepted'=>'Accepted',
        'rejected'=>'Rejected',
        'canceled'=>'Canceled',
    ],

];
