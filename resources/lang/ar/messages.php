<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'object_not_found' => 'الهدف غير موجود !',
    'deleted_successful' => 'تم الحذف بنجاح',
    'created_successful' => 'تم الاضافة بنجاح',
    'updated_successful' => 'تم الحفظ بنجاح',
    'rated_successful' => 'تم التقييم بنجاح',
    'unfav_successful' => 'تم الازالة من المفضلة',
    'fav_successful' => 'تم الاضافة للمفضلة',
    'unfollow_successful' => 'تم ازالة المتابعة بنجاح',
    'follow_successful' => 'تم المتابعة بنجاح',
    'deleted_successfully'=>'تم الحذف بنجاح !',
    'saved_successfully'=>'تم الحفظ بنجاح !',
    'wrong_data'=>'الإدخال خاطئ !',
    'offer_exists'=>'لا يمكنك اضافة اكثر من عرض للهدف الواحد !',
    'payment_method_not_allowed'=>'طريقة الدفع غير متاحة !',
    'date_reserved'=>'هذا التاريخ محجوز مسبقا !',
    'offer_expired'=>'هذا العرض منتهي !',
    'wrong_sequence'=>'ترتيب خاطئ !',
    'dont_have_permission'=>'لا تمتلك صلاحيات لهذا الاجراء !',


];
