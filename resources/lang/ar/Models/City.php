<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'cities' => 'المدن',
    'city' => 'مدينة',
    'the_city' => 'المدينة',
    'name' => 'الاسم بالانجليزي',
    'name_ar' => 'الاسم بالعربي',
    'country_id' => 'الدولة',
    'area' => 'المنطقة',
    'Areas' => [
        'E'=>'الشرق',
        'W'=>'الغرب',
        'S'=>'الجنوب',
        'N'=>'الشمال',
        'M'=>'الوسط',
    ],
    'is_active' => 'الحالة',
];
