<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'settings' => 'الاعدادات',
    'setting' => 'اعداد',
    'the_setting' => 'الاعداد',
    'key' => 'الاعداد',
    'name' => 'الاسم بالانجليزي',
    'name_ar' => 'الاسم بالعربي',
    'value' => 'المحتوى',
    'value_ar' => 'المحتوى بالعربي',
];
