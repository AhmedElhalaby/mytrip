<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'providers' => 'المزودين',
    'provider' => 'مزود',
    'the_provider' => 'المزود',

    'name' => 'الاسم',
    'email' => 'البريد الالكتروني',
    'mobile' => 'رقم الجوال',
    'image' => 'الصورة',
    'city_id' => 'المدينة',
    'gender' => 'الجنس',
    'dob' => 'تاريخ الميلاد',
    'address' => 'العنوان',
    'password' => 'كلمة المرور',
    'password_confirmation' => 'تأكيد كلمة المرور',
    'Gender'=>[
        'male'=>'ذكر',
        'female'=>'انثى',
    ],
    'is_active' => 'الحالة',

];
