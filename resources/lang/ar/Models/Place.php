<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'places' => 'الأماكن',
    'place' => 'مكان',
    'the_place' => 'المكان',
    'name' => 'الاسم',
    'description' => 'الوصف',
    'address' => 'العنوان',
    'lat' => 'الاحداثي العرضي',
    'lng' => 'الاحداثي الطولي',
    'time_in' => 'وقت الدخول',
    'time_out' => 'وقت الخروج',
    'price' => 'السعر',
    'category_id' => 'النوع',
    'user_id' => 'المزود',
    'city_id' => 'المدينة',
    'images' => 'الصور',
    'is_active' => 'الحالة',

];
