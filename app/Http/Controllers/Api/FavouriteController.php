<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Favourite\DestroyForm;
use App\Http\Requests\Api\Favourite\IndexForm;
use App\Http\Requests\Api\Favourite\StoreForm;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class FavouriteController extends Controller
{
    use ResponseTrait;

    /**
     * @param IndexForm $form
     * @return JsonResponse
     */
    public function index(IndexForm $form)
    {
        return $form->persist();
    }

    /**
     * @param StoreForm $form
     * @return JsonResponse
     */
    public function store(StoreForm $form)
    {
        return $form->persist();
    }


    /**
     * @param DestroyForm $form
     * @return JsonResponse
     */
    public function destroy(DestroyForm $form)
    {
        return $form->persist();
    }

}
