<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Place\DestroyForm;
use App\Http\Requests\Api\Place\DestroyImageForm;
use App\Http\Requests\Api\Place\IndexForm;
use App\Http\Requests\Api\Place\StoreForm;
use App\Http\Requests\Api\Place\UpdateFeatureForm;
use App\Http\Requests\Api\Place\UpdateForm;
use App\Http\Requests\Api\Place\RateForm;
use App\Models\CategoryFeature;
use App\Models\Order;
use App\Models\OrderReservation;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class PlaceController extends Controller
{
    use ResponseTrait;

    /**
     * @param IndexForm $form
     * @return JsonResponse
     */
    public function index(IndexForm $form){
        return $form->persist();
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id){
        $Place = Place::find($id);
        if($Place != null){
            $CategoryFeatures = [];
            foreach (CategoryFeature::where('category_id',$Place->category_id)->get() as $feature){
                $PlaceFeature = PlaceFeature::where('category_feature_id',$feature->id)->where('place_id',$Place->id)->first();
                if($PlaceFeature != null){
                    $feature['category_feature_id'] = $PlaceFeature->category_feature_id;
                    $feature['place_id'] = $PlaceFeature->place_id;
                    $feature['price'] = $PlaceFeature->price;
                    $feature['available'] = true;
                }else{
                    $feature['available'] = false;
                }
                $CategoryFeatures[] = $feature;
            }
            $Orders = Order::where('place_id',$Place->id)->pluck('id');
            $OrderReservations = OrderReservation::whereIn('order_id',$Orders)->pluck('reservation_date');
            $Place->setAttribute('CategoryFeatures', $CategoryFeatures);
            $Place->setAttribute('order_reservations', $OrderReservations);
        }
        return $this->successJsonResponse([],$Place,'Place');
    }

    /**
     * @param StoreForm $form
     * @return JsonResponse
     */
    public function store(StoreForm $form){
        return $form->persist();
    }

    /**
     * @param UpdateForm $form
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdateForm $form, $id){
        return $form->persist();
    }

    /**
     * @param UpdateFeatureForm $form
     * @return JsonResponse
     */
    public function update_feature(UpdateFeatureForm $form){
        return $form->persist();
    }

    /**
     * @param RateForm $form
     * @return JsonResponse
     */
    public function rate(RateForm $form){
        return $form->persist();
    }

    /**
     * @param DestroyForm $form
     * @param $id
     * @return JsonResponse
     */
    public function destroy(DestroyForm $form,$id){
        return $form->persist($id);
    }

    /**
     * @param DestroyImageForm $form
     * @return JsonResponse
     */
    public function destroy_image(DestroyImageForm $form){
        return $form->persist();
    }
}
