<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Comment\DestroyForm;
use App\Http\Requests\Api\Comment\IndexForm;
use App\Http\Requests\Api\Comment\StoreForm;
use App\Http\Requests\Api\Comment\UpdateForm;
use App\Models\Comment;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class CommentController extends Controller
{
    use ResponseTrait;

    /**
     * @param IndexForm $form
     * @return JsonResponse
     */
    public function index(IndexForm $form)
    {
        return $form->persist();
    }

    /**
     * @param $place_id
     * @param $id
     * @return JsonResponse
     */
    public function show($place_id,$id)
    {
        return $this->successJsonResponse([], Comment::find($id), 'Comment');
    }

    /**
     * @param StoreForm $form
     * @return JsonResponse
     */
    public function store(StoreForm $form)
    {
        return $form->persist();
    }

    /**
     * @param UpdateForm $form
     * @return JsonResponse
     */
    public function update(UpdateForm $form)
    {
        return $form->persist();
    }


    /**
     * @param DestroyForm $form
     * @return JsonResponse
     */
    public function destroy(DestroyForm $form)
    {
        return $form->persist();
    }

}
