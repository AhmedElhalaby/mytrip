<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ContactForm;
use App\Master;
use App\Models\Category;
use App\Models\City;
use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\Order;
use App\Models\OrderReservation;
use App\Models\PaymentMethod;
use App\Models\Place;
use App\Models\Setting;
use App\Models\Utility;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class HomeController extends Controller
{
    use ResponseTrait;
    public function install(){
        $data = [];
        $data['Cities'] = City::where('is_active',true)->get();
        $data['Categories'] = Category::where('is_active',true)->get();
        $data['PaymentMethod'] = PaymentMethod::all();
        $data['PlaceSpecialFor'] = Place::special_for_Api();
        $data['Utilities'] = Utility::all();
        $data['Setting'] = Setting::all()->pluck((app()->getLocale() == 'ar')?'value_ar':'value', 'key')->toArray();
        return $this->successJsonResponse([],$data,'data');
    }
    public function home(){
        $data = [];
        $data['Shalet_count'] = Place::where('user_id',auth()->user()->id)->where('category_id',1)->count();
        $data['Resort_count'] = Place::where('user_id',auth()->user()->id)->where('category_id',2)->count();
        $Places = Place::where('user_id',auth()->user()->id)->pluck('id');
        $data['Offer_count'] = Offer::whereIn('place_id',$Places)->count();
        $data['OrderNew'] = Order::whereIn('place_id',$Places)->where('status',Order::STATUS['New'])->count();
        $data['OrderAccepted'] = Order::whereIn('place_id',$Places)->where('status',Order::STATUS['Accept'])->count();
        $data['OrderRejected'] = Order::whereIn('place_id',$Places)->where('status',Order::STATUS['Reject'])->count();
        $data['OrderFinished'] = Order::whereIn('place_id',$Places)->where('status',Order::STATUS['Accept'])->where('leave_at','<',now()->format('Y-m-d'))->count();
        return $this->successJsonResponse([],$data,'data');
    }
    public function datesReserved(){
        $data = [];
        $Places = Place::where('user_id',auth()->user()->id)->get();
        foreach ($Places as $place) {
            $Orders = Order::where('place_id',$place->id)->where('status','!=',Order::STATUS['Cancel'])->pluck('id');
            $OrderReservation = OrderReservation::whereIn('order_id',$Orders)->pluck('reservation_date');
            $reservations = [
                'name'=> $place->name,
                'reservation'=>$OrderReservation
            ];
            array_push($data,$reservations);
        }
        return $this->successJsonResponse([],$data,'Reservations');
    }

    /**
     * @param ContactForm $form
     * @return JsonResponse
     */
    public function contact_us(ContactForm $form){
        return $form->persist();
    }
}
