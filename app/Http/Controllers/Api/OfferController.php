<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Offer\DestroyForm;
use App\Http\Requests\Api\Offer\IndexForm;
use App\Http\Requests\Api\Offer\StoreForm;
use App\Http\Requests\Api\Offer\UpdateForm;
use App\Models\Comment;
use App\Models\Offer;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class OfferController extends Controller
{
    use ResponseTrait;

    /**
     * @param IndexForm $form
     * @return JsonResponse
     */
    public function index(IndexForm $form)
    {
        return $form->persist();
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->successJsonResponse([], Offer::find($id), 'Offer');
    }

    /**
     * @param StoreForm $form
     * @return JsonResponse
     */
    public function store(StoreForm $form)
    {
        return $form->persist();
    }

    /**
     * @param UpdateForm $form
     * @return JsonResponse
     */
    public function update(UpdateForm $form)
    {
        return $form->persist();
    }


    /**
     * @param DestroyForm $form
     * @return JsonResponse
     */
    public function destroy(DestroyForm $form)
    {
        return $form->persist();
    }

}
