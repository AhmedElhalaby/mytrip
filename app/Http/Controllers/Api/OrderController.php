<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Order\DestroyForm;
use App\Http\Requests\Api\Order\IndexForm;
use App\Http\Requests\Api\Order\StoreForm;
use App\Http\Requests\Api\Order\UpdateForm;
use App\Models\Order;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    use ResponseTrait;

    /**
     * @param IndexForm $form
     * @return JsonResponse
     */
    public function index(IndexForm $form)
    {
        return $form->persist();
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->successJsonResponse([], Order::find($id), 'Order');
    }

    /**
     * @param StoreForm $form
     * @return JsonResponse
     */
    public function store(StoreForm $form)
    {
        return $form->persist();
    }

    /**
     * @param UpdateForm $form
     * @return JsonResponse
     */
    public function update(UpdateForm $form)
    {
        return $form->persist();
    }
}
