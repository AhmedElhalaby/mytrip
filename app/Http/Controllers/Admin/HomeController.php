<?php

namespace App\Http\Controllers\Admin;

use App\Master;
use App\Models\Place;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{

    /**
     * @return Factory|View
     */
    public function index(){
        return view('admin.home');
    }
    public function general_notification(Request $request){
        $Title = $request->has('title')?$request->title:'';
        $Message = $request->has('msg')?$request->msg:'';
        $Users = new User();
        if($request->has('type') && $request->type == 1)
            $Users = $Users->where('type',User::TYPE['Customer']);
        if($request->has('type') && $request->type == 2)
            $Users = $Users->where('type',User::TYPE['Provider']);
        if($request->filled('city')){
            $Users = $Users->where('city_id',$request->city);
        }
        if($request->filled('category')){
            $Places = Place::where('category_id',$request->category)->pluck('user_id');
            $Users = $Users->whereIn('id',$Places);
        }
        foreach ($Users->get() as $user){
            if($user->device_token != null)
                Master::sendNotification($user->id,$user->device_token,$Title,$Message,null,Notification::TYPE['General']);
        }
        return redirect()->back()->with('status', __('admin.messages.notification_sent'));
    }

}
