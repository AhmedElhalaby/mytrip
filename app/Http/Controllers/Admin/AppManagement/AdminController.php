<?php

namespace App\Http\Controllers\Admin\AppManagement;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Admin\ActivationRequest;
use App\Http\Requests\Admin\Admin\DestroyRequest;
use App\Http\Requests\Admin\Admin\ExportRequest;
use App\Http\Requests\Admin\Admin\SearchRequest;
use App\Http\Requests\Admin\Admin\StoreRequest;
use App\Http\Requests\Admin\Admin\UpdatePasswordRequest;
use App\Http\Requests\Admin\Admin\UpdateRequest;
use App\Master;
use App\Models\Admin;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AdminController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_create = 'ahmedmelhalaby.crud.create';
    protected $view_edit = 'ahmedmelhalaby.crud.edit';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Admin.admins',
        'TheName'=>'Models/Admin.the_admin',
        'Name'=>'Models/Admin.admin',
        'redirect'=>'admin/app_managements/admins',
    ];

    public static function Columns()
    {
        return array(
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/Admin.name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
            'email'=> array(
                'name'=>'email',
                'lng_name'=>'Models/Admin.email',
                'type'=>'email',
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
            'is_active'=> array(
                'name'=>'is_active',
                'lng_name'=>'Models/Admin.is_active',
                'type'=>'is_active',
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
        );
    }

    public static function Fields()
    {
        return array(
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/Admin.name',
                'type'=>'text',
                'is_required'=>true
            ),
            'email'=> array(
                'name'=>'email',
                'lng_name'=>'Models/Admin.email',
                'type'=>'email',
                'is_required'=>true
            ),
            'password'=> array(
                'name'=>'password',
                'lng_name'=>'Models/Admin.password',
                'type'=>'password',
                'confirmation'=>true,
                'editable'=>false,
                'is_required'=>true
            )
        );
    }

    public static function Links()
    {
        return array(
            'edit',
            'activation',
            'change_password',
            'delete',
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $Fields = $this->Fields();
        return view($this->view_create,compact('Fields'))->with($this->Params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Object = Admin::find($id);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Object = Admin::find($id);
        $Fields = $this->Fields();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object','Fields'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }

    /**
     * Update the Password resource in storage.
     *
     * @param UpdatePasswordRequest $request
     * @return UpdatePasswordRequest
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @return ActivationRequest
     */
    public function activation(ActivationRequest $request, $id)
    {
        return  $request->preset($this->Params['redirect']);
    }
}
