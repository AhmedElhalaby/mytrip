<?php

namespace App\Http\Controllers\Admin\AppData;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Place\ActivationRequest;
use App\Http\Requests\Admin\Place\DestroyRequest;
use App\Http\Requests\Admin\Place\ExportRequest;
use App\Http\Requests\Admin\Place\SearchRequest;
use App\Http\Requests\Admin\Place\StoreRequest;
use App\Http\Requests\Admin\Place\UpdateRequest;
use App\Master;
use App\Models\Category;
use App\Models\City;
use App\Models\Place;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class PlaceController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Place.places',
        'TheName'=>'Models/Place.the_place',
        'Name'=>'Models/Place.place',
        'redirect'=>'admin/app_data/places',
        'create'=>false,
    ];
    public static function Columns()
    {
        return array(
            'name'=> array(
                'name'=>(app()->getLocale() == 'ar')?'name_ar':'name',
                'lng_name'=>'Models/Place.name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
            'category_id' => array(
                'name' => 'category_id',
                'lng_name' => 'Models/Place.category_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> Category::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'category'
                ],
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'user_id' => array(
                'name' => 'user_id',
                'lng_name' => 'Models/Place.user_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> User::where('type',User::TYPE['Provider'])->get(),
                    'name'=> 'name',
                    'entity'=>'user',
                    'url'=>'admin/app_users/providers'
                ],
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'city_id' => array(
                'name' => 'city_id',
                'lng_name' => 'Models/Place.city_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> City::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'city'
                ],
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'price'=> array(
                'name'=>'price',
                'lng_name'=>'Models/Place.price',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
            'is_active'=> array(
                'name'=>'is_active',
                'lng_name'=>'Models/Place.is_active',
                'type'=>'is_active',
                'is_searchable'=>true,
                'order'=>true
            ),
        );
    }

    public static function Fields()
    {
        return array(
            'category_id' => array(
                'name' => 'category_id',
                'lng_name' => 'Models/Place.category_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> Category::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'category'
                ],
                'is_required'=>true
            ),
            'city_id' => array(
                'name' => 'city_id',
                'lng_name' => 'Models/Place.city_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> City::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'city'
                ],
                'is_required'=>true
            ),
            'user_id' => array(
                'name' => 'user_id',
                'lng_name' => 'Models/Place.user_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> User::all(),
                    'name'=>'name',
                    'entity'=>'user'
                ],
                'is_required'=>true
            ),
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/Place.name',
                'type'=>'text',
                'is_required'=>true
            ),
            'description'=> array(
                'name'=>'description',
                'lng_name'=>'Models/Place.description',
                'type'=>'text',
                'is_required'=>true
            ),
            'address'=> array(
                'name'=>'address',
                'lng_name'=>'Models/Place.address',
                'type'=>'text',
                'is_required'=>true
            ),
            'lat'=> array(
                'name'=>'lat',
                'lng_name'=>'Models/Place.lat',
                'type'=>'number',
                'is_required'=>true
            ),
            'lng'=> array(
                'name'=>'lng',
                'lng_name'=>'Models/Place.lng',
                'type'=>'number',
                'is_required'=>true
            ),
            'time_in'=> array(
                'name'=>'time_in',
                'lng_name'=>'Models/Place.time_in',
                'type'=>'time',
                'is_required'=>true
            ),
            'time_out'=> array(
                'name'=>'time_out',
                'lng_name'=>'Models/Place.time_out',
                'type'=>'time',
                'is_required'=>true
            ),
            'price'=> array(
                'name'=>'price',
                'lng_name'=>'Models/Place.price',
                'type'=>'number',
                'is_required'=>true
            ),
            'special_for'=> array(
                'name'=>'special_for',
                'lng_name'=>'Models/Place.special_for',
                'type'=>'select',
                'data'=>[
                    Place::$special_for['Families']=>__('Models/Place.SpecialFor.Families'),
                    Place::$special_for['Singles']=>__('Models/Place.SpecialFor.Singles'),
                    Place::$special_for['Marrieds']=>__('Models/Place.SpecialFor.Marrieds'),
                    Place::$special_for['All']=>__('Models/Place.SpecialFor.All'),
                ],
                'is_required'=>true
            ),
            'kids_allowed'=> array(
                'name'=>'kids_allowed',
                'lng_name'=>'Models/Place.kids_allowed',
                'type'=>'boolean',
                'is_required'=>true
            ),
            'Events_allowed'=> array(
                'name'=>'Events_allowed',
                'lng_name'=>'Models/Place.Events_allowed',
                'type'=>'boolean',
                'is_required'=>true
            ),
            'people_count'=> array(
                'name'=>'people_count',
                'lng_name'=>'Models/Place.people_count',
                'type'=>'number',
                'is_required'=>true
            ),
            'place_code'=> array(
                'name'=>'place_code',
                'lng_name'=>'Models/Place.place_code',
                'type'=>'text',
                'is_required'=>true
            ),
            'images'=> array(
                'name'=>'images',
                'lng_name'=>'Models/Place.images',
                'type'=>'images',
                'is_required'=>true
            ),
        );
    }

    public static function Links()
    {
        return array(
            'show',
            'activation',
            'delete',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Object = Place::find($id);
        $Fields = $this->Fields();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object','Fields'))->with($this->Params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @return ActivationRequest
     */
    public function activation(ActivationRequest $request, $id)
    {
        return  $request->preset($this->Params['redirect']);
    }
}
