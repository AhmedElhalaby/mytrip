<?php

namespace App\Http\Controllers\Admin\AppData;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Order\ActivationRequest;
use App\Http\Requests\Admin\Order\DestroyRequest;
use App\Http\Requests\Admin\Order\ExportRequest;
use App\Http\Requests\Admin\Order\SearchRequest;
use App\Http\Requests\Admin\Order\StoreRequest;
use App\Http\Requests\Admin\Order\UpdateRequest;
use App\Master;
use App\Models\Category;
use App\Models\City;
use App\Models\Order;
use App\Models\Place;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class OrderController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Order.orders',
        'TheName'=>'Models/Order.the_order',
        'Name'=>'Models/Order.order',
        'redirect'=>'admin/app_data/orders',
        'create'=>false,
    ];
    public static function Columns()
    {
        return array(
            'place_id'=> array(
                'name'=>'place_id',
                'lng_name'=>'Models/Order.place_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> Place::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'place'
                ],
                'is_searchable'=>true,
                'order'=>true,
                'export' => true,
            ),
            'user_id' => array(
                'name' => 'user_id',
                'lng_name' => 'Models/Order.user_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> User::where('type',User::TYPE['Provider'])->get(),
                    'name'=> 'name',
                    'entity'=>'user',
                ],
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'enter_at' => array(
                'name' => 'enter_at',
                'lng_name' => 'Models/Order.enter_at',
                'type' => 'date',
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'leave_at' => array(
                'name' => 'leave_at',
                'lng_name' => 'Models/Order.leave_at',
                'type' => 'date',
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'status' => array(
                'name' => 'status',
                'lng_name' => 'Models/Order.status',
                'type' => 'select',
                'data'=>[
                    Order::STATUS['New'] => __('Models/Order.Statuses.new'),
                    Order::STATUS['Accept'] => __('Models/Order.Statuses.accepted'),
                    Order::STATUS['Reject'] => __('Models/Order.Statuses.rejected'),
                    Order::STATUS['Cancel'] => __('Models/Order.Statuses.canceled'),
                ],
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
        );
    }

    public static function Fields()
    {
        return array();
    }

    public static function Links()
    {
        return array(

        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

}
