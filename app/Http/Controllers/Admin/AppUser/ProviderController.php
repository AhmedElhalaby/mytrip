<?php

namespace App\Http\Controllers\Admin\AppUser;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Provider\ActivationRequest;
use App\Http\Requests\Admin\Provider\DestroyRequest;
use App\Http\Requests\Admin\Provider\ExportRequest;
use App\Http\Requests\Admin\Provider\SearchRequest;
use App\Http\Requests\Admin\Provider\StoreRequest;
use App\Http\Requests\Admin\Provider\UpdatePasswordRequest;
use App\Http\Requests\Admin\Provider\UpdateRequest;
use App\Master;
use App\Models\Admin;
use App\Models\Category;
use App\Models\City;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ProviderController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_create = 'ahmedmelhalaby.crud.create';
    protected $view_edit = 'ahmedmelhalaby.crud.edit';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Provider.providers',
        'TheName'=>'Models/Provider.the_provider',
        'Name'=>'Models/Provider.provider',
        'redirect'=>'admin/app_users/providers',
    ];

    public static function Columns()
    {
        return array(
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/Provider.name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
            'email'=> array(
                'name'=>'email',
                'lng_name'=>'Models/Provider.email',
                'type'=>'email',
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
            'mobile'=> array(
                'name'=>'mobile',
                'lng_name'=>'Models/Provider.mobile',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
            'city_id'=> array(
                'name'=>'city_id',
                'lng_name'=>'Models/Provider.city_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> City::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'city'
                ],
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
            'is_active'=> array(
                'name'=>'is_active',
                'lng_name'=>'Models/Provider.is_active',
                'type'=>'is_active',
                'is_searchable'=>true,
                'order'=>true,
                'export'=>true,
            ),
        );
    }

    public static function Fields()
    {
        return array(
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/Provider.name',
                'type'=>'text',
                'is_required'=>true
            ),
            'email'=> array(
                'name'=>'email',
                'lng_name'=>'Models/Provider.email',
                'type'=>'email',
                'is_required'=>true
            ),
            'mobile'=> array(
                'name'=>'mobile',
                'lng_name'=>'Models/Provider.mobile',
                'type'=>'text',
                'is_required'=>true
            ),
            'address'=> array(
                'name'=>'address',
                'lng_name'=>'Models/Provider.address',
                'type'=>'text',
                'is_required'=>false
            ),
            'city_id'=> array(
                'name'=>'city_id',
                'lng_name'=>'Models/Provider.city_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> City::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'city'
                ],
                'is_required'=>true
            ),
            'dob'=> array(
                'name'=>'dob',
                'lng_name'=>'Models/Provider.dob',
                'type'=>'date',
                'is_required'=>false
            ),
            'gender'=> array(
                'name'=>'gender',
                'lng_name'=>'Models/Provider.gender',
                'type'=>'select',
                'data'=>[
                    User::GENDER['Male']=>__('Models/User.Gender.male'),
                    User::GENDER['Female']=>__('Models/User.Gender.female')
                ],
                'is_required'=>true
            ),
            'password'=> array(
                'name'=>'password',
                'lng_name'=>'Models/Provider.password',
                'type'=>'password',
                'confirmation'=>true,
                'editable'=>false,
                'is_required'=>true
            ),
            'bank_name'=> array(
                'name'=>'bank_name',
                'lng_name'=>'Models/Provider.bank_name',
                'type'=>'text',
                'is_required'=>true
            ),
            'iban_number'=> array(
                'name'=>'iban_number',
                'lng_name'=>'Models/Provider.iban_number',
                'type'=>'text',
                'is_required'=>true
            ),
            'account_number'=> array(
                'name'=>'account_number',
                'lng_name'=>'Models/Provider.account_number',
                'type'=>'text',
                'is_required'=>true
            )
        );
    }

    public static function Links()
    {
        return array(
            'edit',
            'activation',
            'change_password',
            'delete',
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $Fields = $this->Fields();
        return view($this->view_create,compact('Fields'))->with($this->Params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Object = User::find($id);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Object = User::find($id);
        $Fields = $this->Fields();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object','Fields'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }

    /**
     * Update the Password resource in storage.
     *
     * @param UpdatePasswordRequest $request
     * @return UpdatePasswordRequest
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @return ActivationRequest
     */
    public function activation(ActivationRequest $request, $id)
    {
        return  $request->preset($this->Params['redirect']);
    }
}
