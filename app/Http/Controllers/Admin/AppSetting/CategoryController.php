<?php

namespace App\Http\Controllers\Admin\AppSetting;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Category\ActivationRequest;
use App\Http\Requests\Admin\Category\DestroyRequest;
use App\Http\Requests\Admin\Category\ExportRequest;
use App\Http\Requests\Admin\Category\SearchRequest;
use App\Http\Requests\Admin\Category\StoreRequest;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Master;
use App\Models\Category;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CategoryController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_create = 'ahmedmelhalaby.crud.create';
    protected $view_edit = 'ahmedmelhalaby.crud.edit';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Category.categories',
        'TheName'=>'Models/Category.the_category',
        'Name'=>'Models/Category.category',
        'redirect'=>'admin/app_settings/categories',
    ];

    public static function Columns()
    {
        return array(
            'name'=> array(
                'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                'lng_name'=>'Models/Category.name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
            'is_active'=> array(
                'name'=>'is_active',
                'lng_name'=>'Models/Category.is_active',
                'type'=>'is_active',
                'is_searchable'=>true,
                'order'=>true
            ),
        );
    }

    public static function Fields()
    {
        return array(
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/Category.name',
                'type'=>'text',
                'is_required'=>true
            ),
            'name_ar'=> array(
                'name'=>'name_ar',
                'lng_name'=>'Models/Category.name_ar',
                'type'=>'text',
                'is_required'=>true
            ),
            'description'=> array(
                'name'=>'description',
                'lng_name'=>'Models/Category.description',
                'type'=>'text',
                'is_required'=>true
            ),
            'description_ar'=> array(
                'name'=>'description_ar',
                'lng_name'=>'Models/Category.description_ar',
                'type'=>'text',
                'is_required'=>true
            ),
        );
    }

    public static function Links()
    {
        return array(
            'edit',
            'activation',
            'delete',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $Fields = $this->Fields();
        return view($this->view_create,compact('Fields'))->with($this->Params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Object = Category::find($id);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Object = Category::find($id);
        $Fields = $this->Fields();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object','Fields'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @return ActivationRequest
     */
    public function activation(ActivationRequest $request, $id)
    {
        return  $request->preset($this->Params['redirect']);
    }
}
