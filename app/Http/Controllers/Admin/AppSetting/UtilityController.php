<?php

namespace App\Http\Controllers\Admin\AppSetting;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Utility\ActivationRequest;
use App\Http\Requests\Admin\Utility\DestroyRequest;
use App\Http\Requests\Admin\Utility\ExportRequest;
use App\Http\Requests\Admin\Utility\SearchRequest;
use App\Http\Requests\Admin\Utility\StoreRequest;
use App\Http\Requests\Admin\Utility\UpdateRequest;
use App\Master;
use App\Models\Utility;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class UtilityController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_create = 'ahmedmelhalaby.crud.create';
    protected $view_edit = 'ahmedmelhalaby.crud.edit';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Utility.utilities',
        'TheName'=>'Models/Utility.the_utility',
        'Name'=>'Models/Utility.utility',
        'redirect'=>'admin/app_settings/utilities',
    ];

    public static function Columns()
    {
        return array(
            'name'=> array(
                'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                'lng_name'=>'Models/Utility.name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
        );
    }

    public static function Fields()
    {
        return array(
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/Utility.name',
                'type'=>'text',
                'is_required'=>true
            ),
            'name_ar'=> array(
                'name'=>'name_ar',
                'lng_name'=>'Models/Utility.name_ar',
                'type'=>'text',
                'is_required'=>true
            ),
        );
    }

    public static function Links()
    {
        return array(
            'edit',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $Fields = $this->Fields();
        return view($this->view_create,compact('Fields'))->with($this->Params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return RedirectResponse|Redirector
     */
    public function store(StoreRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $Object = Utility::find($id);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $Object = Utility::find($id);
        $Fields = $this->Fields();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object','Fields'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return RedirectResponse|Redirector
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return RedirectResponse|Redirector
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @return RedirectResponse|Redirector
     */
    public function activation(ActivationRequest $request, $id)
    {
        return  $request->preset($this->Params['redirect']);
    }
}
