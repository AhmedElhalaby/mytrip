<?php

namespace App\Http\Controllers\Admin\AppSetting;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\City\ActivationRequest;
use App\Http\Requests\Admin\City\DestroyRequest;
use App\Http\Requests\Admin\City\ExportRequest;
use App\Http\Requests\Admin\City\SearchRequest;
use App\Http\Requests\Admin\City\StoreRequest;
use App\Http\Requests\Admin\City\UpdateRequest;
use App\Master;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class CityController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_create = 'ahmedmelhalaby.crud.create';
    protected $view_edit = 'ahmedmelhalaby.crud.edit';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/City.cities',
        'TheName'=>'Models/City.the_city',
        'Name'=>'Models/City.city',
        'redirect'=>'admin/app_settings/cities',
    ];

    public static function Columns()
    {
        return array(
            'country_id' => array(
                'name' => 'country_id',
                'lng_name' => 'Models/City.country_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> Country::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'country'
                ],
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'name'=> array(
                'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                'lng_name'=>'Models/City.name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
            'is_active'=> array(
                'name'=>'is_active',
                'lng_name'=>'Models/City.is_active',
                'type'=>'is_active',
                'is_searchable'=>true,
                'order'=>true
            ),
        );
    }

    public static function Fields()
    {
        return array(
            'country_id' => array(
                'name' => 'country_id',
                'lng_name' => 'Models/City.country_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> Country::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'country'
                ],
                'is_required'=>true
            ),
            'name'=> array(
                'name'=>'name',
                'lng_name'=>'Models/City.name',
                'type'=>'text',
                'is_required'=>true
            ),
            'name_ar'=> array(
                'name'=>'name_ar',
                'lng_name'=>'Models/City.name_ar',
                'type'=>'text',
                'is_required'=>true
            ),
            'area'=> array(
                'name'=>'area',
                'lng_name'=>'Models/City.area',
                'type'=>'select',
                'data'=>[
                    City::$Area['E']=>__('Models/City.Areas.E'),
                    City::$Area['S']=>__('Models/City.Areas.S'),
                    City::$Area['W']=>__('Models/City.Areas.W'),
                    City::$Area['N']=>__('Models/City.Areas.N'),
                    City::$Area['M']=>__('Models/City.Areas.M'),
                ],
                'is_required'=>true
            ),
        );
    }

    public static function Links()
    {
        return array(
            'edit',
            'activation',
            'delete',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $Fields = $this->Fields();
        return view($this->view_create,compact('Fields'))->with($this->Params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return RedirectResponse|Redirector
     */
    public function store(StoreRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Object = City::find($id);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Object = City::find($id);
        $Fields = $this->Fields();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object','Fields'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @return ActivationRequest
     */
    public function activation(ActivationRequest $request, $id)
    {
        return  $request->preset($this->Params['redirect']);
    }
}
