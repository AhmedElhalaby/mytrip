<?php

namespace App\Http\Controllers\Admin\AppSetting;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Contact\DestroyRequest;
use App\Http\Requests\Admin\Contact\ExportRequest;
use App\Http\Requests\Admin\Contact\SearchRequest;
use App\Master;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ContactController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Contact.contacts',
        'TheName'=>'Models/Contact.the_contact',
        'Name'=>'Models/Contact.contact',
        'redirect'=>'admin/app_settings/contacts',
        'create'=>false,
    ];

    public static function Columns()
    {
        return array(
            'user_id' => array(
                'name' => 'user_id',
                'lng_name' => 'Models/Contact.user_id',
                'type' => 'relation',
                'relation'=>[
                    'data'=> User::all(),
                    'name'=>'name',
                    'entity'=>'user'
                ],
                'is_searchable' => true,
                'order' => true,
                'export' => true,
            ),
            'name'=> array(
                'name'=>'title',
                'lng_name'=>'Models/Contact.title',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
            'message'=> array(
                'name'=>'message',
                'lng_name'=>'Models/Contact.message',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
        );
    }

    public static function Links()
    {
        return array(
            'delete',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }
}
