<?php

namespace App\Http\Controllers\Admin\AppSetting;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\Setting\SearchRequest;
use App\Http\Requests\Admin\Setting\UpdateRequest;
use App\Master;
use App\Models\Category;
use App\Models\Setting;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SettingController extends Controller
{
    protected $view_index = 'ahmedmelhalaby.crud.index';
    protected $view_show = 'ahmedmelhalaby.crud.show';
    protected $view_create = 'ahmedmelhalaby.crud.create';
    protected $view_edit = 'ahmedmelhalaby.crud.edit';
    protected $view_export = 'ahmedmelhalaby.crud.export';
    protected $Params = [
        'Names'=>'Models/Setting.settings',
        'TheName'=>'Models/Setting.the_setting',
        'Name'=>'Models/Setting.setting',
        'redirect'=>'admin/app_settings/settings',
        'create'=>false
    ];

    public static function Columns()
    {
        return array(
            'key'=> array(
                'name'=>'key',
                'lng_name'=>'Models/Setting.key',
                'type'=>'text',
                'is_searchable'=>false,
                'order'=>false
            ),
            'value'=> array(
                'name'=>(app()->getLocale()=='ar')?'value_ar':'value',
                'lng_name'=>'Models/Setting.name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ),
        );
    }

    public static function Fields()
    {
        return array(
            'value'=> array(
                'name'=>'value',
                'lng_name'=>'Models/Setting.value',
                'type'=>'textarea',
                'is_required'=>true
            ),
            'value_ar'=> array(
                'name'=>'value_ar',
                'lng_name'=>'Models/Setting.value_ar',
                'type'=>'textarea',
                'is_required'=>true
            ),
        );
    }

    public static function Links()
    {
        return array(
            'edit',
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Object = Setting::find($id);
        $Fields = $this->Fields();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object','Fields'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }

}
