<?php

namespace App\Http\Requests\Api\Favourite;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Favourite;
use App\Models\Place;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class IndexForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }


    /**
     * @return JsonResponse
     */
    public function persist()
    {
        $Favourites = new Favourite();
        $Favourites = $Favourites->where('user_id',$this->user()->id)->orderBy('created_at','desc')->pluck('place_id');
        $Objects = new Place();
        $Objects = $Objects->whereIn('id',$Favourites);
        $Objects = $Objects->paginate($this->has('per_page')?$this->per_page:10);
        return $this->successJsonResponse( [],$Objects->items(),'Places',$Objects);

    }

}
