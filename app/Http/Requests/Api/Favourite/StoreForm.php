<?php

namespace App\Http\Requests\Api\Favourite;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Favourite;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class StoreForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place_id' => 'required|exists:places,id',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return Master::NiceNames('Favourite');
    }

    /**
     * @return JsonResponse
     */
    public function persist()
    {
        $Favourite = Favourite::where('user_id',$this->user()->id)->where('place_id',$this->place_id)->first();
        if($Favourite != null){
            $Favourite->delete();
            return $this->successJsonResponse( [__('messages.unfav_successful')]);
        }
        $Object = new Favourite();
        $Object->user_id = $this->user()->id;
        $Object->place_id = $this->place_id;
        $Object->save();
        return $this->successJsonResponse( [__('messages.fav_successful')]);

    }

}
