<?php

namespace App\Http\Requests\Api\Place;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\OrderReservation;
use App\Models\Place;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class IndexForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('q') && ($this->q == '' || $this->q == null))
            unset($data['q']);
        if($this->has('city_id') && ($this->city_id == '' || $this->city_id == null))
            unset($data['city_id']);
        if($this->has('category_id') && ($this->category_id == '' || $this->category_id == null))
            unset($data['category_id']);
        if($this->has('order_by') && ($this->order_by == '' || $this->order_by == null))
            unset($data['order_by']);
        if($this->has('date') && ($this->date == '' || $this->date == null))
            unset($data['date']);
        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'q'=>'string',
            'city_id'=>'exists:cities,id',
            'category_id'=>'exists:categories,id',
            'order_by'=>'in:rate,created_at',
            'date'=>'date'
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $Objects = new Place();
        $logged = auth('api')->user();
        if($logged && $logged->type == User::TYPE['Provider'])
            $Objects = $Objects->where('user_id',$logged->id);
        if($this->has('q'))
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orWhere('place_code','LIKE','%'.$this->q.'%');
        if($this->has('city_id'))
            $Objects = $Objects->where('city_id',$this->city_id);
        if($this->has('category_id'))
            $Objects = $Objects->where('category_id',$this->category_id);
        if($this->has('order_by')){
            switch ($this->order_by){
                case 'rate':{
                    $Objects = $Objects->orderByRaw("(select AVG(rate) from rates where rates.place_id = places.id) DESC");
                    break;
                }
                case 'created_at':{
                    $Objects = $Objects->orderBy('created_at','desc');
                    break;
                }
                default :
                    break;
            }
        }
        if($this->filled('date')){
            $Dates = OrderReservation::whereDate('reservation_date',$this->date)->pluck('order_id');
            $Objects = $Objects->whereNotIn('id',$Dates);
        }
        $Objects = $Objects->paginate($this->has('per_page')?$this->per_page:10);
        return $this->successJsonResponse( [],$Objects->items(),'Places',$Objects);

    }

}
