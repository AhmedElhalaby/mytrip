<?php

namespace App\Http\Requests\Api\Place;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class DestroyForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist($id)
    {
        $Object = Place::find($id);
        if($Object == null)
            return $this->failJsonResponse([__('messages.object_not_found')],'','data',null,404);
        $Object->delete();
        return $this->successJsonResponse( [__('messages.deleted_successful')]);
    }

}
