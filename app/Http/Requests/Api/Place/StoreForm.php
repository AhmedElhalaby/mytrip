<?php

namespace App\Http\Requests\Api\Place;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\City;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Models\PlacePaymentMethod;
use App\Models\PlaceUtility;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class StoreForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('address') && ($this->address == '' || $this->address == null)){
            unset($data['address']);
        }
        if($this->has('lat') && ($this->lat == '' || $this->lat == null)){
            unset($data['lat']);
        }
        if($this->has('lng') && ($this->lng == '' || $this->lng == null)){
            unset($data['lng']);
        }
        if($this->has('time_in') && ($this->time_in == '' || $this->time_in == null)){
            unset($data['time_in']);
        }
        if($this->has('time_out') && ($this->time_out == '' || $this->time_out == null)){
            unset($data['time_out']);
        }
        if($this->has('video_url') && ($this->video_url == '' || $this->video_url == null)){
            unset($data['video_url']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|exists:categories,id',
            'city_id' => 'required|exists:cities,id',
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'price' => 'required|numeric',
            'address' => 'string|max:255',
            'lat' => 'string|max:255',
            'lng' => 'string|max:255',
            'time_in' => 'string|max:255',
            'time_out' => 'string|max:255',
            'place_features'=>'required|array|min:1',
            'place_features.*.category_feature_id'=>'required|exists:category_features,id',
            'image' => 'required',
            'neighborhood' => 'required|string|max:255',
            'special_for' => 'required|numeric',
            'kids_allowed' => 'required|boolean',
            'Events_allowed' => 'required|boolean',
            'people_count' => 'required|numeric',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'place_payment_method'=>'required|array|min:1',
            'place_payment_method.*'=>'required|exists:payment_methods,id',
            'utilities'=>'array|min:1',
            'utilities.*'=>'exists:utilities,id',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        if($this->user()->type != User::TYPE['Provider'])
            return $this->failJsonResponse([__('messages.dont_have_permission')]);
        $city = City::find($this->city_id);
        $Object = new Place();
        $Object->user_id = $this->user()->id;
        $Object->name = $this->name;
        $Object->description = $this->description;
        $Object->city_id = $this->city_id;
        $Object->category_id = $this->category_id;
        $Object->price = $this->price;
        $Object->address = @$this->address;
        $Object->lat = @$this->lat;
        $Object->lng = @$this->lng;
        $Object->time_in = @$this->time_in;
        $Object->time_out = @$this->time_out;
        $Object->video_url = @$this->video_url;
        $Object->neighborhood = @$this->neighborhood;
        $Object->special_for = @$this->special_for;
        $Object->kids_allowed = @$this->kids_allowed;
        $Object->Events_allowed = @$this->Events_allowed;
        $Object->people_count = @$this->people_count;
//        /////////////////////////////////////
        $Object->boards = @$this->boards;
        $Object->boards_primary_size = @$this->boards_primary_size;
        $Object->boards_extra_size = @$this->boards_extra_size;
        $Object->boards_outer_size = @$this->boards_outer_size;
        $Object->boards_extension_size = @$this->boards_extension_size;
        $Object->bedrooms = @$this->bedrooms;
        $Object->kitchen_table_size = @$this->kitchen_table_size;
        $Object->kitchen_oven = @$this->kitchen_oven;
        $Object->kitchen_refrigerator = @$this->kitchen_refrigerator;
        $Object->kitchen_microwave = @$this->kitchen_microwave;
        $Object->kitchen_coffee_machine = @$this->kitchen_coffee_machine;
        $Object->bathrooms = @$this->bathrooms;
        $Object->bathroom_wipes = @$this->bathroom_wipes;
        $Object->bathroom_soap = @$this->bathroom_soap;
        $Object->bathroom_bath_tub = @$this->bathroom_bath_tub;
        $Object->bathroom_shampoo = @$this->bathroom_shampoo;
        $Object->bathroom_Bathrobe = @$this->bathroom_Bathrobe;
        $Object->bathroom_shower = @$this->bathroom_shower;
//        /////////////////////////////////////
        $Object->save();
        $Object->place_code = $city->country->country_code.$city->area.$Object->id;
        $Object->save();
        foreach ($this->place_features as $item){
            PlaceFeature::create(['category_feature_id'=>$item['category_feature_id'],'place_id'=>$Object->id,'price'=>@$item['price']]);
        }
        foreach ($this->place_payment_method as $payment_method){
            PlacePaymentMethod::create(['place_id'=>$Object->id,'payment_method_id'=>$payment_method]);
        }
        if ($this->filled('utilities')){
            foreach ($this->utilities as $utility){
                PlaceUtility::create(['utility_id'=>$utility,'place_id'=>$Object->id]);
            }
        }
        Master::MultiUpload('image','Places',$Object->id);
        return $this->successJsonResponse( [__('messages.created_successful')],$Object,'Place');

    }

}
