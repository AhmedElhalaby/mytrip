<?php

namespace App\Http\Requests\Api\Place;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Notification;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Models\Rate;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class RateForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('review') && ($this->review == '' || $this->review == null)){
            unset($data['review']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate' => 'required|numeric|max:5',
            'review' => 'string|max:255',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $Place = Place::find($this->route('id'));
        if($Place == null)
            return $this->failJsonResponse([__('messages.object_not_found')],'','data',null,404);
        $Object = Rate::where('place_id',$this->route('id'))->where('user_id',$this->user()->id)->first();
        if($Object == null)
            $Object = new Rate();
        $Object->place_id = $this->route('id');
        $Object->user_id = $this->user()->id;
        $Object->rate = $this->rate;
        $Object->review = @$this->review;
        $Object->save();
        if($Place->user->device_token != null)
            Master::SendNotification($Place->user_id,$Place->user->device_token,__('تقييم جديد'),__('قام '.$this->user()->name.'بتقييم ('.$Place->name.') الخاص بك '),$Place->id,Notification::TYPE['Rate']);
        return $this->successJsonResponse( [__('messages.created_successful')]);
    }

}
