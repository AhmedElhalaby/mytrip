<?php

namespace App\Http\Requests\Api\Place;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Models\PlacePaymentMethod;
use App\Models\PlaceUtility;
use App\Models\Utility;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UpdateForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('category_id') && ($this->category_id == '' || $this->category_id == null)){
            unset($data['category_id']);
        }
        if($this->has('city_id') && ($this->city_id == '' || $this->city_id == null)){
            unset($data['city_id']);
        }
        if($this->has('name') && ($this->name == '' || $this->name == null)){
            unset($data['name']);
        }
        if($this->has('description') && ($this->description == '' || $this->description == null)){
            unset($data['description']);
        }
        if($this->has('address') && ($this->address == '' || $this->address == null)){
            unset($data['address']);
        }
        if($this->has('price') && ($this->price == '' || $this->price == null)){
            unset($data['price']);
        }
        if($this->has('lat') && ($this->lat == '' || $this->lat == null)){
            unset($data['lat']);
        }
        if($this->has('lng') && ($this->lng == '' || $this->lng == null)){
            unset($data['lng']);
        }
        if($this->has('time_in') && ($this->time_in == '' || $this->time_in == null)){
            unset($data['time_in']);
        }
        if($this->has('time_out') && ($this->time_out == '' || $this->time_out == null)){
            unset($data['time_out']);
        }
        if($this->has('image') && ($this->image == '' || $this->image == null)){
            unset($data['image']);
        }
        if($this->has('place_features') && ($this->place_features == '' || $this->place_features == null)){
            unset($data['place_features']);
        }
        if($this->has('place_payment_method') && ($this->place_payment_method == '' || $this->place_payment_method == null)){
            unset($data['place_payment_method']);
        }
        if($this->has('place_payment_method') && ($this->place_payment_method == '' || $this->place_payment_method == null)){
            unset($data['place_payment_method']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'exists:categories,id',
            'city_id' => 'exists:cities,id',
            'name' => 'string|max:255',
            'description' => 'string|max:255',
            'price' => 'numeric',
            'address' => 'string|max:255',
            'lat' => 'string|max:255',
            'lng' => 'string|max:255',
            'time_in' => 'string|max:255',
            'time_out' => 'string|max:255',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'place_features'=>'array|min:1',
            'place_features.*.category_feature_id'=>'exists:category_features,id',
            'place_payment_method'=>'array|min:1',
            'place_payment_method.*'=>'exists:payment_methods,id',
            'utilities'=>'array|min:1',
            'utilities.*'=>'exists:utilities,id',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $Object = Place::find($this->route('place'));
        if($Object == null)
            return $this->failJsonResponse([__('messages.object_not_found')],'','data',null,404);
        $data = $this->all();
        if($this->has('image')){
            Master::MultiUpload('image','Places',$Object->id);
            unset($data['image']);
        }
        if($this->has('place_payment_method')){
            foreach ($this->place_payment_method as $payment_method){
                PlacePaymentMethod::firstOrCreate(['place_id'=>$Object->id,'payment_method_id'=>$payment_method]);
            }
        }
        $Object->update($data);
        if($this->has('place_features')){
            PlaceFeature::where('place_id',$Object->id)->delete();
            foreach ($this->place_features as $item){
                PlaceFeature::create(['category_feature_id'=>$item['category_feature_id'],'place_id'=>$Object->id,'price'=>@$item['price']]);
            }
        }
        if($this->has('utilities')){
            PlaceUtility::where('place_id',$Object->id)->delete();
            foreach ($this->utilities as $utility){
                PlaceUtility::create(['utility_id'=>$utility,'place_id'=>$Object->id]);
            }
        }
        return $this->successJsonResponse( [__('messages.updated_successful')],$Object,'Place');
    }

}
