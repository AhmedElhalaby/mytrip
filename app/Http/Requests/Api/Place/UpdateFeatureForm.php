<?php

namespace App\Http\Requests\Api\Place;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UpdateFeatureForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('price') && ($this->price == '' || $this->price == null)){
            unset($data['price']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:1,2,3',
            'category_feature_id' => 'required|exists:category_features,id',
            'price' => 'numeric|max:255',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $Object = Place::find($this->route('id'));
        if($Object == null)
            return $this->failJsonResponse([__('messages.object_not_found')],'','data',null,404);
        switch ($this->type){
            case 1:{//Add New
                $PlaceFeature = PlaceFeature::where('place_id',$this->route('id'))->where('category_feature_id',$this->id)->first();
                if($PlaceFeature ==null){
                    PlaceFeature::create(['place_id'=>$this->route('id'),'category_feature_id'=>$this->category_feature_id,'price'=>@$this->price]);
                }
                break;
            }
            case 2:{//Update
                $PlaceFeature = PlaceFeature::where('place_id',$this->route('id'))->where('category_feature_id',$this->id)->first();
                $PlaceFeature->update(['price'=>@$this->price]);
                break;
            }
            case 3:{//Remove
                $PlaceFeature = PlaceFeature::where('place_id',$this->route('id'))->where('category_feature_id',$this->id)->first();
                if($PlaceFeature ==null){
                    $PlaceFeature->delete();
                }
                break;
            }
            default :
                break;
        }
        return $this->successJsonResponse( [__('messages.updated_successful')],$Object,'Place');
    }

}
