<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\Offer;
use App\Models\Order;
use App\Models\OrderFeature;
use App\Models\OrderReservation;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Models\PlacePaymentMethod;
use App\Traits\ResponseTrait;
use App\User;
use Carbon\CarbonPeriod;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class StoreForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('description') && ($this->description == '' || $this->description == null)){
            unset($data['description']);
        }
        if($this->has('payment_details') && ($this->payment_details == '' || $this->payment_details == null)){
            unset($data['payment_details']);
        }
        if($this->has('place_features') && ($this->place_features == '' || $this->place_features == null)){
            unset($data['place_features']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place_id' => 'required|exists:places,id',
            'offer_id' => 'exists:offers,id',
            'payment_method_id' => 'required|exists:payment_methods,id',
            'enter_at' => 'required|date|max:255',
            'leave_at' => 'required|date|max:255',
            'description' => 'string|max:255',
            'payment_details' => 'string|max:255',
            'place_features'=>'array',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $Place = Place::find($this->place_id);
        $DatesPeriod = [];
        $price = $Place->price;
        //Payment Method Validation
        $PlacePaymentMethod = PlacePaymentMethod::where('place_id',$this->place_id)->where('payment_method',$this->payment_method_id);
        if($PlacePaymentMethod == null)
            return $this->failJsonResponse([__('messages.payment_method_not_allowed')]);
        //Reserved Date Validation
        $Order = Order::where('place_id',$Place->id)->whereIn('status',[Order::STATUS['New'],Order::STATUS['Accept']])->where('leave_at','>',now()->format('Y-m-d'))->pluck('id');
        $OrderReservation = OrderReservation::whereIn('order_id',$Order)->pluck('reservation_date');
        $CarbonPeriod = CarbonPeriod::create(Carbon::parse($this->enter_at)->format('Y-m-d'), Carbon::parse($this->leave_at)->format('Y-m-d'));
        foreach ($CarbonPeriod as $period){
            $DatesPeriod[]=$period->format('Y-m-d');
        }
        $similarity = array_intersect($OrderReservation->toArray(), $DatesPeriod);
        if(count($similarity)>0){
            return $this->failJsonResponse([__('messages.date_reserved')]);
        }
        //Check Offer Expiration
        if($this->has('offer_id')){
            $Offer = Offer::find($this->offer_id);
            if($Offer->expired_at < now()->format('Y-m-d'))
                return $this->failJsonResponse([__('messages.offer_expired')]);
            else
                $price = $price *($Offer->price /100);
        }
        $price = $price * count($DatesPeriod);
        //Store The Order
        $Object = new Order();
        $Object->user_id = $this->user()->id;
        $Object->place_id = $this->place_id;
        $Object->offer_id = @$this->offer_id;
        $Object->payment_method_id = $this->payment_method_id;
        $Object->enter_at = Carbon::parse($this->enter_at)->format('Y-m-d');
        $Object->leave_at = Carbon::parse($this->leave_at)->format('Y-m-d');
        $Object->description = @$this->description;
        $Object->payment_details = @$this->payment_details;
        $Object->user_name = @$this->user_name;
        $Object->user_email = @$this->user_email;
        $Object->user_mobile = @$this->user_mobile;
        $Object->price = $price;
        if($Place->user_id == $this->user()->id){
            $Object->status = Order::STATUS['Accept'];
        }
        $Object->save();
        //Store The Dates Period
        foreach ($DatesPeriod as $date){
            OrderReservation::create(['order_id'=>$Object->id,'reservation_date'=>$date]);
        }
        //Total Price Calculated
        if($this->has('place_features')){
            $price = $Object->price;
            foreach ($this->place_features as $item){
                $PlaceFeature = PlaceFeature::where('category_feature_id',$item)->where('place_id',$Place->id)->first();
                if($PlaceFeature != null){
                    OrderFeature::create([
                        'order_id'=>$Object->id,
                        'place_feature_id'=>$PlaceFeature->id,
                        'price'=>$PlaceFeature->price,
                        'name'=>$PlaceFeature->category_feature->name,
                        'name_ar'=>$PlaceFeature->category_feature->name_ar,
                        'description'=>$PlaceFeature->category_feature->description,
                        'description_ar'=>$PlaceFeature->category_feature->description_ar,
                        'image'=>$PlaceFeature->category_feature->image,
                    ]);
                    $price +=$PlaceFeature->price;
                }
            }
            if($this->has('offer_id')){
                $Offer = Offer::find($this->offer_id);
                $price = $price *($Offer->price /100);
            }
            $Object->price = $price;
            $Object->save();
        }
        if($Place->user_id != $this->user()->id)
            Master::SendNotification($Place->user_id,@$Place->user->device_token,'حجز جديد','يوجد لديك حجز جديد',$Object->id,Notification::TYPE['Order']);
        return $this->successJsonResponse( [__('messages.created_successful')],$Object,'Order');
    }
}
