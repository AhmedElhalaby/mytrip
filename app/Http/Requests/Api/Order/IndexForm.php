<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Comment;
use App\Models\Order;
use App\Models\Place;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;

class IndexForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('status') && ($this->status == '' || $this->status == null)){
            unset($data['status']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $logged = $this->user();
        $Objects = new Order();
        if($logged->type == User::TYPE['Customer']){
            $Objects = $Objects->where('user_id',$logged->id);
        }
        elseif ($logged->type == User::TYPE['Provider']){
            $Places = Place::where('user_id',$logged->id)->pluck('id');
            $Objects = $Objects->whereIn('place_id',$Places);
        }
        if ($this->has('status')){
            $Objects = $Objects->where('status',$this->status);
        }
        $Objects = $Objects->orderBy('created_at','desc')->paginate($this->has('per_page')?$this->per_page:10);
        return $this->successJsonResponse( [],$Objects->items(),'Orders',$Objects);

    }

}
