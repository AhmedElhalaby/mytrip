<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\Order;
use App\Traits\ResponseTrait;

class UpdateForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|in:'.Order::STATUS['Accept'].','.Order::STATUS['Reject'].','.Order::STATUS['Cancel'],
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $Object = Order::find($this->route('order'));
        if($Object == null)
            return $this->failJsonResponse([__('messages.object_not_found')],'','data',null,404);
        switch ($this->status){
            case Order::STATUS['Accept']:{
                if($Object->status != Order::STATUS['New'])
                    return $this->failJsonResponse([__('messages.wrong_sequence')]);
                $Object->status = Order::STATUS['Accept'];
                Master::SendNotification($Object->user_id,@$Object->user->device_token,'تم قبول الحجز','تم قبول الحجز بنجاح',$Object->id,Notification::TYPE['Order']);
                break;
            }
            case Order::STATUS['Reject']:{
                if($Object->status != Order::STATUS['New'])
                    return $this->failJsonResponse([__('messages.wrong_sequence')]);
                $Object->status = Order::STATUS['Reject'];
                $Object->reject_reason = @$this->reject_reason;
                Master::SendNotification($Object->user_id,@$Object->user->device_token,'تم رفض الحجز','تم رفض طلب الحجز الخاص بك',$Object->id,Notification::TYPE['Order']);
                break;
            }
            case Order::STATUS['Cancel']:{
                if($Object->status == (Order::STATUS['Cancel']||Order::STATUS['Reject']))
                    return $this->failJsonResponse([__('messages.wrong_sequence')]);
                $Object->status = Order::STATUS['Cancel'];
                $Object->cancel_reason = @$this->cancel_reason;
                Master::SendNotification($Object->place->user_id,@$Object->place->user->device_token,'تم الغاء الحجز','تم الغاء الحجز',$Object->id,Notification::TYPE['Order']);
                break;
            }
        }
        $Object->save();
        return $this->successJsonResponse( [__('messages.updated_successful')],$Object,'Comment');
    }

}
