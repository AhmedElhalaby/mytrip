<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\PasswordReset;
use App\Notifications\PasswordReset as PasswordResetNotification;
use App\Traits\ResponseTrait;
use App\Models\User;

class ForgetPasswordRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $user = User::where('email',$this->email)->first();
        if($user){
            $code = rand( 10000 , 99999 );
            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $user->email,
                    'code' => $code
                ]
            );
            $user->notify(
                new PasswordResetNotification($code)
            );
            return $this->successJsonResponse([__('auth.code_sent')] );
        }
        return $this->failJsonResponse([__('messages.object_not_found')]);
    }
}
