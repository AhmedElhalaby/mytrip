<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Traits\ResponseTrait;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\DB;

class UserRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('dob') && ($this->dob == '' || $this->dob == null)){
            unset($data['dob']);
        }
        if($this->has('gender') && ($this->gender == '' || $this->gender == null)){
            unset($data['gender']);
        }
        if($this->has('mobile') && ($this->mobile == '' || $this->mobile == null)){
            unset($data['mobile']);
        }
        if($this->has('city_id') && ($this->city_id == '' || $this->city_id == null)){
            unset($data['city_id']);
        }
        if($this->has('name') && ($this->name == '' || $this->name == null)){
            unset($data['name']);
        }
        if($this->has('email') && ($this->email == '' || $this->email == null)){
            unset($data['email']);
        }
        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255,',
            'email' => 'email|min:6|unique:users,id,'. auth()->user()->id,
            'city_id' => 'exists:cities,id',
            'mobile' => 'numeric|min:6',
            'gender' => 'numeric|in:'.User::GENDER['Male'].','.User::GENDER['Female'].'',
            'dob' => 'sometimes|date',
            'bank_name' => 'sometimes|string',
            'iban_number' => 'sometimes|string',
            'account_number' => 'sometimes|string',
            'device_token' => 'string|required_with:device',
            'device_type' => 'string|required_with:device_token',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = auth()->user();
        $logged->update($this->all());
        $logged->save();
        DB::table('oauth_access_tokens')->where('user_id', $logged->id)->delete();
        $tokenResult = $logged->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $logged['access_token']= $tokenResult->accessToken;
        $logged['token_type']= 'Bearer';
        return $this->successJsonResponse( [__('messages.updated_successful')],$logged,'User');

    }
}
