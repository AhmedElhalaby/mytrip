<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Traits\ResponseTrait;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('dob') && ($this->dob == '' || $this->dob == null)){
            unset($data['dob']);
        }
        if($this->has('gender') && ($this->gender == '' || $this->gender == null)){
            unset($data['gender']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'mobile' => 'required|numeric|min:6',
            'gender' => 'numeric|in:'.User::GENDER['Male'].','.User::GENDER['Female'],
            'email' => 'required|email|min:6|unique:users',
            'city_id' => 'required|exists:cities,id',
            'type' => 'required|in:'.User::TYPE['Customer'].','.User::TYPE['Provider'],
            'dob' => 'sometimes|date',
            'bank_name' => 'sometimes|string',
            'iban_number' => 'sometimes|string',
            'account_number' => 'sometimes|string',
            'device_token' => 'string|required_with:device',
            'device_type' => 'string|required_with:device_token',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $user = new User();
        $user->name = $this->name;
        $user->city_id = $this->city_id;
        $user->password = Hash::make($this->password);
        $user->mobile = $this->mobile;
        $user->email = $this->email;
        $user->gender = ($this->gender)?$this->gender:1;
        $user->dob = @$this->dob;
        $user->bank_name = @$this->bank_name;
        $user->iban_number = @$this->iban_number;
        $user->account_number = @$this->account_number;
        $user->address = @$this->address;
        $user->type = $this->type;
        if ($this->input('device_token')) {
            $user->device_token = $this->device_token;
            $user->device_type = $this->device_type;
        }
        $user->save();
        Auth::attempt(request(['email', 'password']));
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->refresh();
        $user['access_token']= $tokenResult->accessToken;
        $user['token_type']= 'Bearer';
        return $this->successJsonResponse( [__('messages.saved_successfully')],$user,'User');

    }

}
