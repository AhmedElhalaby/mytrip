<?php

namespace App\Http\Requests\Api\Offer;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Comment;
use App\Models\Offer;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class StoreForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('description') && ($this->description == '' || $this->description == null)){
            unset($data['description']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place_id' => 'required|exists:places,id',
            'start_at' => 'required|date',
            'expired_at' => 'required|date',
            'price'=>'required|numeric',
            'name'=>'required|string|max:255',
            'description'=>'string|max:255',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Offer');
    }
    public function persist()
    {
        $Place = Place::find($this->place_id);
        $logged = $this->user();
        if(($logged->type != User::TYPE['Provider']) || ($Place->user_id != $logged->id))
            return $this->failJsonResponse([__('message.object_not_found')],'','data',null,404);
        $Offer = Offer::where('place_id',$Place->id)->where('expired_at','>',now()->format('Y-m-d'))->where('is_active',true)->first();
        if($Offer != null){
            return $this->failJsonResponse([__('messages.offer_exists')]);
        }
        $Object = new Offer();
        $Object->place_id = $this->place_id;
        $Object->start_at = Carbon::parse($this->start_at)->format('Y-m-d');
        $Object->expired_at = Carbon::parse($this->expired_at)->format('Y-m-d');
        $Object->price = $this->price;
        $Object->name = $this->name;
        $Object->description = @$this->description;
        $Object->save();
        return $this->successJsonResponse( [__('messages.created_successful')],$Object,'Offer');

    }

}
