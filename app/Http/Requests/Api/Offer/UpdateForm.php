<?php

namespace App\Http\Requests\Api\Offer;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Comment;
use App\Models\Offer;
use App\Traits\ResponseTrait;
use Illuminate\Contracts\Validation\Validator;

class UpdateForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('description') && ($this->description == '' || $this->description == null)){
            unset($data['description']);
        }
        if($this->has('expired_at') && ($this->expired_at == '' || $this->expired_at == null)){
            unset($data['expired_at']);
        }
        if($this->has('start_at') && ($this->start_at == '' || $this->start_at == null)){
            unset($data['start_at']);
        }
        if($this->has('price') && ($this->price == '' || $this->price == null)){
            unset($data['price']);
        }
        if($this->has('name') && ($this->name == '' || $this->name == null)){
            unset($data['name']);
        }
        if($this->has('description') && ($this->description == '' || $this->description == null)){
            unset($data['description']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_at' => 'date',
            'expired_at' => 'date',
            'price'=>'numeric',
            'name'=>'string|max:255',
            'description'=>'string|max:255',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Offer');
    }
    public function persist()
    {

        $Object = Offer::find($this->route('offer'));
        if($Object == null)
            return $this->failJsonResponse([__('messages.object_not_found')],'','data',null,404);
        $Object->update($this->all());
        return $this->successJsonResponse( [__('messages.updated_successful')],$Object,'Offer');
    }

}
