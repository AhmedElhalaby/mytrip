<?php

namespace App\Http\Requests\Api\Offer;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Offer;
use App\Traits\ResponseTrait;

class DestroyForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Offer');
    }
    public function persist()
    {
        $Object = Offer::find($this->route('offer'));
        if($Object == null)
            return $this->failJsonResponse([__('messages.object_not_found')],'','data',null,404);
        $Object->delete();
        return $this->successJsonResponse( [__('messages.deleted_successful')]);
    }

}
