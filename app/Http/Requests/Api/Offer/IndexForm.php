<?php

namespace App\Http\Requests\Api\Offer;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Offer;
use App\Models\Place;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;

class IndexForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * @return Validator
     */
    protected function getValidatorInstance()
    {
        $data = $this->all();
        if($this->has('place_id') && ($this->place_id == '' || $this->place_id == null)){
            unset($data['place_id']);
        }
        if($this->has('city_id') && ($this->city_id == '' || $this->city_id == null)){
            unset($data['city_id']);
        }
        $this->getInputSource()->replace($data);
        /*modify data before send to validator*/
        return parent::getValidatorInstance();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place_id'=>'exists:places,id',
            'city_id'=>'exists:cities,id',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Offer');
    }
    public function persist()
    {
        if(auth('api')->check() && auth('api')->user()->type == User::TYPE['Provider']){
            $Places = Place::where('user_id',auth('api')->user()->id)->pluck('id');
            $Places = Offer::whereIn('place_id',$Places)->where('expired_at','>',now()->format('Y-m-d'))->where('is_active',true)->pluck('place_id');
        }else{
            $Places = Offer::where('expired_at','>',now()->format('Y-m-d'))->where('is_active',true)->pluck('place_id');
        }
        $Objects = new Place();
        $Objects = $Objects->whereIn('id',$Places)->orderBy('created_at','desc');
        $Objects = $Objects->paginate($this->has('per_page')?$this->per_page:10);
        return $this->successJsonResponse( [],$Objects->items(),'Places',$Objects);

    }

}
