<?php

namespace App\Http\Requests\Api\Comment;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Order;
use App\Models\Place;
use App\Traits\ResponseTrait;
use App\User;

class IndexForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $Objects = new Order();
        if($this->user()->type == User::TYPE['Customer'])
            $Objects = $Objects->where('user_id',$this->user()->id);
        elseif ($this->user()->type == User::TYPE['Provider']){
            $Places = Place::where('user_id',$this->user()->id)->pluck('id');
            $Objects = $Objects->whereIn('id',$Places);
        }
        $Objects = $Objects->paginate($this->has('per_page')?$this->per_page:10);
        return $this->successJsonResponse( [],$Objects->items(),'Orders',$Objects);

    }

}
