<?php

namespace App\Http\Requests\Api\Comment;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Comment;
use App\Traits\ResponseTrait;

class UpdateForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'required|string|max:255',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Comment');
    }
    public function persist()
    {
        $Object = Comment::find($this->route('comment'));
        if($Object == null)
            return $this->failJsonResponse([__('message.object_not_found')],'','data',null,404);
        $Object->update($this->all());
        return $this->successJsonResponse( [__('messages.updated_successful')],$Object,'Comment');
    }

}
