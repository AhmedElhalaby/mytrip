<?php

namespace App\Http\Requests\Api\Comment;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Comment;
use App\Models\Place;
use App\Models\PlaceFeature;
use App\Traits\ResponseTrait;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class StoreForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => 'required|string|max:255',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Comment');
    }
    public function persist()
    {
        if(!Place::find($this->route('place_id')))
            return $this->failJsonResponse([__('message.object_not_found')],'','data',null,404);
        $Object = new Comment();
        $Object->user_id = $this->user()->id;
        $Object->place_id = $this->route('place_id');
        $Object->comment = $this->comment;
        $Object->save();
        return $this->successJsonResponse( [__('messages.created_successful')],$Object,'Comment');

    }

}
