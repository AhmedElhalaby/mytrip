<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\Contact;
use App\Models\Favourite;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class ContactForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'message' => 'required|string',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return Master::NiceNames('Favourite');
    }

    /**
     * @return JsonResponse
     */
    public function persist()
    {
        $Object = new Contact();
        $Object->user_id = $this->user()->id;
        $Object->title = $this->title;
        $Object->message = $this->message;
        $Object->save();
        return $this->successJsonResponse( [__('messages.created_successful')]);

    }

}
