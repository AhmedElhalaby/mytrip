<?php

namespace App\Http\Requests\Admin\User;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'mobile' => 'required',
            'dob' => 'date',
            'city_id' => 'required|exists:cities,id',
            'gender' => 'required|in:'. User::GENDER['Male'].','.User::GENDER['Female'],
            'email' => 'required|string|max:255|unique:users,email,'.$this->route('user'),
        ];
    }
    public function preset($redirect,$id){
        $Object = User::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->update($this->all());
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
