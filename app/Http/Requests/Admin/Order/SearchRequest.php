<?php

namespace App\Http\Requests\Admin\Order;

use App\Http\Controllers\Admin\AppData\OrderController;
use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Columns = OrderController::Columns();
        $Links = OrderController::Links();
        $Objects = new Order();
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%');
        }
        if($this->filled('user_id')){
            $Objects = $Objects->where('user_id',$this->user_id);
        }
        if($this->filled('place_id')){
            $Objects = $Objects->where('place_id',$this->place_id);
        }
        if($this->filled('status')){
            $Objects = $Objects->where('status',$this->status);
        }
        if($this->filled('order_by') && $this->filled('order_type')){
            $Objects = $Objects->orderBy($this->order_by,$this->order_type);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects','Columns','Links'))->with($params);
    }
}
