<?php

namespace App\Http\Requests\Admin\Feature;

use App\Models\Category;
use App\Models\CategoryFeature;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'name_ar' => 'required|string|max:255',
            'description' => 'string|max:255',
            'description_ar' => 'string|max:255',
            'category_id' => 'required|exists:categories,id',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
        ];
    }
    public function preset($redirect){
        $Object = CategoryFeature::create(array(
            'name' => $this->name,
            'name_ar' => $this->name_ar,
            'description' => $this->description,
            'description_ar' => $this->description_ar,
            'category_id' => $this->category_id,
            'image' => $this->image,
        ));
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
