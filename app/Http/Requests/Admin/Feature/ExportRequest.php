<?php

namespace App\Http\Requests\Admin\Feature;

use App\Exports\CategoryFeatureExport;
use App\Http\Controllers\Admin\AppSetting\FeatureController;
use App\Models\CategoryFeature;
use Illuminate\Foundation\Http\FormRequest;
use Maatwebsite\Excel\Facades\Excel;

class ExportRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't' => 'required|in:print,pdf,xls',
        ];
    }
    public function preset($view,$params){
        $Columns = FeatureController::Columns();
        $Links = FeatureController::Links();
        $Objects   = CategoryFeature::all();
        $ext = $this->t;
        if ($ext == 'print')
            return view($view)->with(['Objects' => $Objects,'Columns' => $Columns,'Links' => $Links, 'names' => $params['Names'], 'print' => true]);
        elseif ($ext == 'pdf')
            return (new \App\Master)->exportPDF($Objects, $view, $params['Names'],$Columns);
        elseif($ext == 'xls')
            if(count($Objects) > 0)
                return Excel::download(new CategoryFeatureExport(),now().'.xlsx');
    }
}
