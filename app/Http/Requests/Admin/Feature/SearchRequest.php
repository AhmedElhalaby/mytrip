<?php

namespace App\Http\Requests\Admin\Feature;

use App\Http\Controllers\Admin\AppSetting\FeatureController;
use App\Models\Category;
use App\Models\CategoryFeature;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Columns = FeatureController::Columns();
        $Links = FeatureController::Links();
        $Objects = new CategoryFeature();
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orWhere('name_ar','LIKE','%'.$this->q.'%');
        }
        if($this->filled('name')){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%')->orWhere('name_ar','LIKE','%'.$this->name.'%');
        }
        if($this->filled('category_id')){
            $Objects = $Objects->where('category_id',$this->category_id);
        }
        if($this->filled('is_active')){
            $Objects = $Objects->where('is_active','LIKE','%'.$this->is_active.'%');
        }
        if($this->filled('order_by') && $this->filled('order_type')){
            $Objects = $Objects->orderBy($this->order_by,$this->order_type);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects','Columns','Links'))->with($params);
    }
}
