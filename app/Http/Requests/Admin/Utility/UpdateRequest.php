<?php

namespace App\Http\Requests\Admin\Utility;

use App\Models\Admin;
use App\Models\Utility;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'name_ar' => 'required|string|max:255',
        ];
    }
    public function preset($redirect,$id){
        $Object = Utility::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->update(array(
            'name' => $this->name,
            'name_ar' => $this->name_ar,
        ));
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
