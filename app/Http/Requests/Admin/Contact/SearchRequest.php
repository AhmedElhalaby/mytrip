<?php

namespace App\Http\Requests\Admin\Contact;

use App\Http\Controllers\Admin\AppSetting\ContactController;
use App\Models\Contact;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Columns = ContactController::Columns();
        $Links = ContactController::Links();
        $Objects = new Contact();
        if($this->has('q')){
            $Objects = $Objects->where('title','LIKE','%'.$this->q.'%')->orwhere('message','LIKE','%'.$this->q.'%');
        }
        if($this->has('title')){
            $Objects = $Objects->where('title','LIKE','%'.$this->title.'%');
        }
        if($this->has('message')){
            $Objects = $Objects->where('message','LIKE','%'.$this->message.'%');
        }
        if($this->has('user_id')){
            $Objects = $Objects->where('user_id',$this->user_id);
        }
        if($this->has('order_by') && $this->has('order_type')){
            $Objects = $Objects->orderBy($this->order_by,$this->order_type);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects','Columns','Links'))->with($params);
    }
}
