<?php

namespace App\Http\Requests\Admin\Contact;

use App\Exports\ContactExport;
use App\Http\Controllers\Admin\AppSetting\ContactController;
use App\Models\Contact;
use Illuminate\Foundation\Http\FormRequest;
use Maatwebsite\Excel\Facades\Excel;

class ExportRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't' => 'required|in:print,pdf,xls',
        ];
    }
    public function preset($view,$params){
        $Columns = ContactController::Columns();
        $Links = ContactController::Links();
        $Objects   = Contact::all();
        $ext = $this->t;
        if ($ext == 'print')
            return view($view)->with(['Objects' => $Objects,'Columns' => $Columns,'Links' => $Links, 'names' => $params['Names'], 'print' => true]);
        elseif ($ext == 'pdf')
            return (new \App\Master)->exportPDF($Objects, $view, $params['Names'],$Columns);
        elseif($ext == 'xls')
            if(count($Objects) > 0)
                return Excel::download(new ContactExport(),now().'.xlsx');
    }
}
