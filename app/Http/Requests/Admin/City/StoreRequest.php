<?php

namespace App\Http\Requests\Admin\City;

use App\Models\City;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'name_ar' => 'required|string|max:255',
            'area' => 'required|string|max:255',
            'country_id' => 'required|exists:countries,id',
        ];
    }
    public function preset($redirect){
        $Object = City::create(array(
            'name' => $this->name,
            'name_ar' => $this->name_ar,
            'country_id' => $this->country_id,
            'area' => $this->area,
        ));
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
