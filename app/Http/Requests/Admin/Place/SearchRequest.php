<?php

namespace App\Http\Requests\Admin\Place;

use App\Http\Controllers\Admin\AppData\PlaceController;
use App\Models\Place;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Columns = PlaceController::Columns();
        $Links = PlaceController::Links();
        $Objects = new Place();
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%');
        }
        if($this->filled('name')){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%');
        }
        if($this->filled('category_id')){
            $Objects = $Objects->where('category_id',$this->category_id);
        }
        if($this->filled('city_id')){
            $Objects = $Objects->where('city_id',$this->city_id);
        }
        if($this->filled('user_id')){
            $Objects = $Objects->where('user_id',$this->user_id);
        }
        if($this->filled('is_active')){
            $Objects = $Objects->where('is_active','LIKE','%'.$this->is_active.'%');
        }
        if($this->filled('price')){
            $Objects = $Objects->where('price','LIKE','%'.$this->price.'%');
        }
        if($this->filled('order_by') && $this->filled('order_type')){
            $Objects = $Objects->orderBy($this->order_by,$this->order_type);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects','Columns','Links'))->with($params);
    }
}
