<?php

namespace App\Http\Requests\Admin\Setting;

use App\Http\Controllers\Admin\AppSetting\SettingController;
use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Columns = SettingController::Columns();
        $Links = SettingController::Links();
        $Objects = new Setting();
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orwhere('name_ar','LIKE','%'.$this->q.'%');
        }
        if($this->has('value')){
            $Objects = $Objects->where('value','LIKE','%'.$this->value.'%');
        }
        if($this->has('value_ar')){
            $Objects = $Objects->where('value_ar','LIKE','%'.$this->value_ar.'%');
        }
        if($this->has('order_by') && $this->has('order_type')){
            $Objects = $Objects->orderBy($this->order_by,$this->order_type);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects','Columns','Links'))->with($params);
    }
}
