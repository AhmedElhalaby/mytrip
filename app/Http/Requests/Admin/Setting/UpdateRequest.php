<?php

namespace App\Http\Requests\Admin\Setting;

use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => 'required|string',
            'value_ar' => 'required|string',
        ];
    }
    public function preset($redirect,$id){
        $Object = Setting::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->update(array(
            'value' => $this->value,
            'value_ar' => $this->value_ar,
        ));
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
