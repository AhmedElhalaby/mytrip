<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','email','mobile','address','city_id','gender','type','dob','verified_at','verification_code','device_token','device_type','password','is_active','bank_name','iban_number','account_number'];
    protected $appends = ['City','notification_count'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified_at' => 'datetime',
    ];

    const TYPE =[
        'Customer'=>1,
        'Provider'=>2
    ];
    const GENDER = [
        'Male'=>1,
        'Female'=>2
    ];
    const AuthType = [
        'default'=>1,
        'facebook'=>2,
        'google'=>3
    ];

    public function city(){
        return $this->belongsTo(City::class);
    }
    public function getCityAttribute(){
        return $this->city()->first();
    }
    public function getNotificationCountAttribute(){
        return Notification::where('user_id',$this->id)->where('read_at',null)->count();
    }
}
