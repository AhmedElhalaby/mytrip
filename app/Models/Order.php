<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'orders';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'place_id','user_id','offer_id','payment_method_id','enter_at','leave_at','status','description','payment_details','price','cancel_reason','user_name','user_email','user_mobile'];
    protected $appends = ['Place','Offer','User','PaymentMethod','order_reservations','CategoryFeatures'];
    public function place(){
        return $this->belongsTo(Place::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function offer(){
        return $this->belongsTo(Offer::class);
    }
    public function payment_method(){
        return $this->belongsTo(PaymentMethod::class);
    }
    public function order_reservations(){
        return $this->hasMany(OrderReservation::class);
    }
    public function order_features(){
        return $this->hasMany(OrderFeature::class);
    }
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }
    const STATUS = [
        'New'=>0,
        'Accept'=>1,
        'Reject'=>2,
        'Cancel'=>3
    ];
    public function getPlaceAttribute(){
        return $this->place()->first();
    }
    public function getOfferAttribute(){
        return $this->offer()->first();
    }
    public function getUserAttribute(){
        return $this->user()->first();
    }
    public function getPaymentMethodAttribute(){
        return $this->payment_method()->first();
    }
    public function getCategoryFeaturesAttribute(){
        return $this->order_features()->get();
    }
    public function getOrderReservationsAttribute(){
        return $this->order_reservations()->pluck('reservation_date');
    }
}
