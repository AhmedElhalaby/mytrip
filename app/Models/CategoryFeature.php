<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFeature extends Model
{

    protected $table = 'category_features';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','name_ar','description','description_ar','category_id','image','is_active'];
    protected $appends = ['final_name','final_description'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function getFinalNameAttribute(){
        return (app()->getLocale() == 'ar')?$this->name_ar:$this->name;
    }

    public function getFinalDescriptionAttribute(){
        return (app()->getLocale() == 'ar')?$this->description_ar:$this->description;
    }

    public function setImageAttribute($value)
    {
        $request = \Request::instance();
        $attribute_name = "image";
        $disk = "storage";
        $destination_path = "category_features/image/";
        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $destination_path.$new_file_name;
        }
    }
}
