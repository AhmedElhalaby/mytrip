<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{

    protected $table = 'favourites';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'place_id','user_id'];

    public function place(){
        return $this->belongsTo(Place::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
