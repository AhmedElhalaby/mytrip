<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlacePaymentMethod extends Model
{

    protected $table = 'place_payment_methods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'place_id','payment_method_id'];

    public function place(){
        return $this->belongsTo(Place::class);
    }
    public function payment_method(){
        return $this->belongsTo(PaymentMethod::class);
    }
}
