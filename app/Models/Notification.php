<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    protected $table = 'notifications';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'user_id','title','message','ref_id','type','read_at'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function order(){
        return $this->belongsTo(Order::class,'ref_id');
    }
    const TYPE = [
        'General'=>0,
        'Order'=>1, // Order Id
        'Rate'=>2 // Place Id
    ];
}
