<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceFeature extends Model
{

    protected $table = 'place_features';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'place_id','category_feature_id','price'];
    protected $appends = ['final_name','final_description'];

    public function place(){
        return $this->belongsTo(Place::class);
    }

    public function category_feature(){
        return $this->belongsTo(CategoryFeature::class);
    }
    public function getFinalNameAttribute(){
        $Category = $this->category_feature()->first();
        return (app()->getLocale() == 'ar')?$Category->name_ar:$Category->name;
    }

    public function getFinalDescriptionAttribute(){
        $Category = $this->category_feature()->first();
        return (app()->getLocale() == 'ar')?$Category->description_ar:$Category->description;
    }

}
