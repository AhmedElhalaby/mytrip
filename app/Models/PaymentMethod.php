<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{

    protected $table = 'payment_methods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','name_ar'];
    protected $appends = ['final_name'];

    public function getFinalNameAttribute(){
        return (app()->getLocale() == 'ar')?$this->name_ar:$this->name;
    }

}
