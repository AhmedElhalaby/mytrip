<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderFeature extends Model
{

    protected $table = 'order_features';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'order_id','place_feature_id','price', 'name','name_ar','description','description_ar','image',];
    protected $appends = ['place_id','category_feature_id','final_name','final_description'];

    public function order(){
        return $this->belongsTo(Order::class);
    }
    public function place_feature(){
        return $this->belongsTo(PlaceFeature::class);
    }
    public function getPlaceIdAttribute(){
        return $this->order()->first()->place_id;
    }
    public function getCategoryFeatureIdAttribute(){
        return @$this->place_feature()->first()->category_feature_id;
    }
    public function getFinalNameAttribute(){
        return (app()->getLocale() == 'ar')?$this->name_ar:$this->name;
    }

    public function getFinalDescriptionAttribute(){
        return (app()->getLocale() == 'ar')?$this->description_ar:$this->description;
    }

}
