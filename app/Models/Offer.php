<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

    protected $table = 'offers';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'place_id','start_at','expired_at','price','name','description','is_active'];

    public function place(){
        return $this->belongsTo(Place::class);
    }


}
