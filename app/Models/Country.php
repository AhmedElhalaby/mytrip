<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $table = 'countries';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','name_ar','country_code','is_active'];

    protected $appends = ['final_name'];
    public function getFinalNameAttribute(){
        return (app()->getLocale()=='ar')?$this->name_ar:$this->name;
    }

}
