<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public static $special_for = [
        'Families'=>1,
        'Singles'=>2,
        'Marrieds'=>3,
        'All'=>4,
    ];
    public static function special_for_Api()
    {
        return [
            [
                'id'=>1,
                'name'=>'For Families',
                'name_ar'=>'للعائلات',
                'final_name'=>(app()->getLocale()=='ar')?'للعائلات':'For Families'
            ],
            [
                'id'=>2,
                'name'=>'For Singles',
                'name_ar'=>'للعازبين فقط',
                'final_name'=>(app()->getLocale()=='ar')?'للعازبين فقط':'For Singles'
            ],
            [
                'id'=>3,
                'name'=>'For Marrieds',
                'name_ar'=>'للمتزوجين فقط ',
                'final_name'=>(app()->getLocale()=='ar')?'للمتزوجين فقط':'For Marrieds'
            ],
            [
                'id'=>4,
                'name'=>'For All',
                'name_ar'=>'للجميع ',
                'final_name'=>(app()->getLocale()=='ar')?'للجميع':'For All'
            ],
        ];
    }
    protected $table = 'places';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','video_url','description','address','lat','lng','time_in','time_out','price','category_id','user_id','city_id','is_active','neighborhood','special_for','kids_allowed','Events_allowed','people_count','place_code','boards','boards_primary_size','boards_extra_size','boards_outer_size','boards_extension_size','bedrooms','kitchen_table_size','kitchen_oven','kitchen_refrigerator','kitchen_microwave','kitchen_coffee_machine','bathrooms','bathroom_wipes','bathroom_soap','bathroom_bath_tub','bathroom_shampoo','bathroom_Bathrobe','bathroom_shower',];
    protected $appends = ['Attachments','PaymentMethods','City','rate','Rates','comments_count','orders_count','offers_count','is_favourite','Offer','offer_price','Utilities'];

    /*
     *************************************************************************************************
     **************************************** Relations **********************************************
     *************************************************************************************************
     */
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function city(){
        return $this->belongsTo(City::class);
    }
    public function rate(){
        return $this->hasMany(Rate::class);
    }
    public function place_features(){
        return $this->hasMany(PlaceFeature::class);
    }
    public function attachments(){
        return $this->hasMany(Attachment::class,'ref_id');
    }
    public function images(){
        return $this->hasMany(Attachment::class,'ref_id');
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function offers(){
        return $this->hasMany(Offer::class);
    }
    public function place_payment_method(){
        return $this->hasMany(PlacePaymentMethod::class);
    }
    public function utilities(){
        return $this->hasMany(PlaceUtility::class);
    }

    /*
     *************************************************************************************************
     **************************************** Getter *************************************************
     *************************************************************************************************
     */

    public function getAttachmentsAttribute(){
        return $this->attachments()->get();
    }
    public function getCityAttribute(){
        return $this->city()->first();
    }
    public function getCommentsCountAttribute(){
        return $this->rate()->count();
    }
    public function getOrdersCountAttribute(){
        return $this->orders()->count();
    }
    public function getOffersCountAttribute(){
        return $this->offers()->count();
    }
    public function getRateAttribute(){
        return ($this->rate()->avg('rate'))?$this->rate()->avg('rate'):0;
    }
    public function getRatesAttribute(){
        return $this->rate()->where('review','!=','')->get();
    }
    public function getUtilitiesAttribute(){
        return Utility::whereIn('id',$this->utilities()->get()->pluck('utility_id'))->get();
    }
    public function getOfferAttribute(){
        $Offer = Offer::where('place_id',$this->id)->where('expired_at','>',now()->format('Y-m-d'))->where('is_active',true)->first();
        return ($Offer)?$Offer:null;
    }
    public function getOfferPriceAttribute(){
        $Offer = Offer::where('place_id',$this->id)->where('expired_at','>',now()->format('Y-m-d'))->where('is_active',true)->first();
        return ($Offer)?($this->price-($this->price * ($Offer->price / 100))):$this->price;
    }
    public function getIsFavouriteAttribute(){
        if(auth('api')->check()){
            if(Favourite::where('place_id',$this->id)->where('user_id',auth('api')->user()->id)->first()){
                return true;
            }
        }
        return false;
    }
    public function getPaymentMethodsAttribute(){
        $PaymentMethod = [];
        foreach ($this->place_payment_method()->get() as $item) {
            $PaymentMethod[] = $item->payment_method;
        }
        return $PaymentMethod;
    }
}
