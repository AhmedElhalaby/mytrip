<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{

    protected $table = 'rates';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'place_id','user_id','rate','review'];
    protected $appends = ['final_name'];
    public function getFinalNameAttribute(){
        return (app()->getLocale()=='ar')?$this->name_ar:$this->name;
    }

}
