<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderReservation extends Model
{

    protected $table = 'order_reservations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'order_id','reservation_date'];

    public function order(){
        return $this->belongsTo(Order::class);
    }
}
