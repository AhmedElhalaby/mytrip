<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public static $Area = [
        'E'=>'E',
        'W'=>'W',
        'S'=>'S',
        'N'=>'N',
        'M'=>'M',
    ];
    protected $table = 'cities';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','name_ar','area','country_id','is_active'];
    protected $appends = ['final_name'];
    public function getFinalNameAttribute(){
        return (app()->getLocale()=='ar')?$this->name_ar:$this->name;
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }

}
