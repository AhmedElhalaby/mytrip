<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceUtility extends Model
{

    protected $table = 'place_utilities';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'place_id','utility_id'];

    public function place()
    {
        return $this->belongsTo(Place::class);
    }
    public function utility(){
        return $this->belongsTo(Utility::class);
    }
}
