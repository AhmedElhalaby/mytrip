<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name','name_ar','description','description_ar','is_active'];
    protected $appends = ['CategoryFeatures','final_name','final_description'];

    public function category_features(){
        return $this->hasMany(CategoryFeature::class);
    }



    public function getCategoryFeaturesAttribute(){
        return $this->category_features()->get();
    }

    public function getFinalNameAttribute(){
        return (app()->getLocale() == 'ar')?$this->name_ar:$this->name;
    }

    public function getFinalDescriptionAttribute(){
        return (app()->getLocale() == 'ar')?$this->description_ar:$this->description;
    }
}
