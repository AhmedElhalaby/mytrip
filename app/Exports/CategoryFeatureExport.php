<?php

namespace App\Exports;

use App\Models\CategoryFeature;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class CategoryFeatureExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return CategoryFeature[]|Collection
    */
    public function collection()
    {
        return CategoryFeature::all();
    }
}
