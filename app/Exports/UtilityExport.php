<?php

namespace App\Exports;

use App\Models\Utility;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class UtilityExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Utility[]|Collection
    */
    public function collection()
    {
        return Utility::all();
    }
}
