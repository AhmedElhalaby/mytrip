<?php

namespace App\Exports;

use App\Models\City;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class CityExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return City[]|Collection
    */
    public function collection()
    {
        return City::all();
    }
}
