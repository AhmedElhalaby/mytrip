<?php

namespace App\Exports;

use App\Models\Place;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class PlaceExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Place[]|Collection
    */
    public function collection()
    {
        return Place::all();
    }
}
