<?php

namespace App\Exports;

use App\Models\Country;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class CountryExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Country[]|Collection
    */
    public function collection()
    {
        return Country::all();
    }
}
