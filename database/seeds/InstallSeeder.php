<?php

use App\Models\Category;
use App\Models\CategoryFeature;
use App\Models\City;
use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class InstallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $City = City::create(['name' => 'Jeddah', 'name_ar' => 'جدة']);
        $Category = Category::create(['name' => 'Shalet', 'name_ar' => 'شاليه', 'description' => 'Shalet', 'description_ar' => 'شاليه']);
        $CategoryFeature = CategoryFeature::create(['name' => 'Pool', 'name_ar' => 'بركة','description'=>'Pool','description_ar'=>'بركة','category_id'=>$Category->id]);
        $PaymentMethod = PaymentMethod::create(['name' => 'Cash', 'name_ar' => 'كاش']);

    }
}
