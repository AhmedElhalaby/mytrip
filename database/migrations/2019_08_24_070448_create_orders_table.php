<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('place_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('offer_id')->nullable();
            $table->unsignedBigInteger('payment_method_id');
            $table->date('enter_at')->nullable();
            $table->date('leave_at')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('description')->nullable();
            $table->string('payment_details')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->string('reject_reason')->nullable();
            $table->float('price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
