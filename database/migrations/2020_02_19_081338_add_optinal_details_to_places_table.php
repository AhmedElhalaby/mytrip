<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptinalDetailsToPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->boolean('boards')->default(false);
            $table->integer('boards_primary_size')->default(0);
            $table->integer('boards_extra_size')->default(0);
            $table->integer('boards_outer_size')->default(0);
            $table->integer('boards_extension_size')->default(0);
            $table->integer('bedrooms')->default(0);
            $table->integer('kitchen_table_size')->default(0);
            $table->boolean('kitchen_oven')->default(false);
            $table->boolean('kitchen_refrigerator')->default(false);
            $table->boolean('kitchen_microwave')->default(false);
            $table->boolean('kitchen_coffee_machine')->default(false);
            $table->integer('bathrooms')->default(0);
            $table->boolean('bathroom_wipes')->default(0);
            $table->boolean('bathroom_soap')->default(0);
            $table->boolean('bathroom_bath_tub')->default(0);
            $table->boolean('bathroom_shampoo')->default(0);
            $table->boolean('bathroom_Bathrobe')->default(0);
            $table->boolean('bathroom_shower')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->dropColumn('boards');
            $table->dropColumn('boards_primary_size');
            $table->dropColumn('boards_extra_size');
            $table->dropColumn('boards_outer_size');
            $table->dropColumn('boards_extension_size');
            $table->dropColumn('bedrooms');
            $table->dropColumn('kitchen_table_size');
            $table->dropColumn('kitchen_oven');
            $table->dropColumn('kitchen_refrigerator');
            $table->dropColumn('kitchen_microwave');
            $table->dropColumn('kitchen_coffee_machine');
            $table->dropColumn('bathrooms');
            $table->dropColumn('bathroom_wipes');
            $table->dropColumn('bathroom_soap');
            $table->dropColumn('bathroom_bath_tub');
            $table->dropColumn('bathroom_shampoo');
            $table->dropColumn('bathroom_Bathrobe');
            $table->dropColumn('bathroom_shower');
        });
    }
}
