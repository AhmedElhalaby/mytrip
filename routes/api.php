<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    Route::group([
        'prefix' => 'auth',
    ], function () {
        Route::post('login', 'AuthController@login');
        Route::post('social_login','AuthController@social_login');
        Route::post('signup', 'AuthController@register');
        Route::post('forget_password', 'AuthController@forget_password');
        Route::post('reset_password', 'AuthController@reset_password');
        Route::group([
            'middleware' => 'auth:api'
        ], function() {
            Route::get('me', 'AuthController@show');
            Route::post('refresh', 'AuthController@refresh');
            Route::post('update', 'AuthController@update');
            Route::post('change_password', 'AuthController@change_password');
            Route::post('logout', 'AuthController@logout');
        });
    });
    Route::get('install','HomeController@install');

    Route::resource('places','PlaceController')->only(['index','show']);
    Route::resource('places/{place_id}/comments','CommentController')->only(['index','show']);
    Route::resource('offers','OfferController')->only(['index','show']);

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('contact_us', 'HomeController@contact_us');
        Route::get('home','HomeController@home');
        Route::get('orders_dates','HomeController@datesReserved');
        Route::resource('places','PlaceController')->only(['store','update','destroy']);
        Route::put('places/{id}/feature', 'PlaceController@update_feature');
        Route::delete('places/{id}/image', 'PlaceController@destroy_image');
        Route::post('places/{id}/rate', 'PlaceController@rate');
        Route::resource('places/{place_id}/comments', 'CommentController')->only(['store','update','destroy']);
        Route::resource('favourites','FavouriteController')->only(['index','store','destroy']);
        Route::resource('orders','OrderController')->only(['index','show','store','update']);
        Route::resource('offers','OfferController')->only(['store','update','destroy']);
        Route::group([
            'prefix' => 'notifications',
        ], function() {
            Route::get('/', 'NotificationController@index');
            Route::post('/read/{id}', 'NotificationController@read');
            Route::post('/read_all', 'NotificationController@read_all');
        });
    });

