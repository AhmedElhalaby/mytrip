<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Admin" middleware group. Enjoy building your Admin!
|
*/


/*
|--------------------------------------------------------------------------
| Admin Auth
|--------------------------------------------------------------------------
| Here is where admin auth routes exists for login and log out
*/
Route::group([
    'namespace'  => 'Auth',
], function() {
    Route::get('login', ['uses' => 'LoginController@showLoginForm','as'=>'admin.login']);
    Route::post('login', ['uses' => 'LoginController@login']);
    Route::group([
        'middleware' => 'App\Http\Middleware\RedirectIfAuthenticatedAdmin',
    ], function() {
        Route::post('logout', ['uses' => 'LoginController@logout','as'=>'admin.logout']);
    });
});
/*
|--------------------------------------------------------------------------
| Admin After login in
|--------------------------------------------------------------------------
| Here is where admin panel routes exists after login in
*/
Route::group([
    'middleware'  => ['App\Http\Middleware\RedirectIfAuthenticatedAdmin'],
], function() {
    Route::get('/', 'HomeController@index');
    Route::post('notification/send', 'HomeController@general_notification');

    /*
    |--------------------------------------------------------------------------
    | Admin > App Management
    |--------------------------------------------------------------------------
    | Here is where App Management routes
    */

    Route::group([
        'prefix'=>'app_managements',
        'namespace'=>'AppManagement',
    ],function () {
        Route::group([
            'prefix'=>'admins'
        ],function () {
            Route::get('/','AdminController@index');
            Route::get('/create','AdminController@create');
            Route::post('/','AdminController@store');
            Route::get('/{admin}','AdminController@show');
            Route::get('/{admin}/edit','AdminController@edit');
            Route::put('/{admin}','AdminController@update');
            Route::delete('/{admin}','AdminController@destroy');
            Route::patch('/update/password',  'AdminController@updatePassword');
            Route::get('/option/export','AdminController@export');
            Route::get('/{id}/activation','AdminController@activation');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Admin > App Settings
    |--------------------------------------------------------------------------
    | Here is where App Settings routes
    */

    Route::group([
        'prefix'=>'app_settings',
        'namespace'=>'AppSetting',
    ],function () {
        Route::group([
            'prefix'=>'categories'
        ],function () {
            Route::get('/','CategoryController@index');
            Route::get('/create','CategoryController@create');
            Route::post('/','CategoryController@store');
            Route::get('/{id}','CategoryController@show');
            Route::get('/{id}/edit','CategoryController@edit');
            Route::put('/{id}','CategoryController@update');
            Route::delete('/{id}','CategoryController@destroy');
            Route::get('/option/export','CategoryController@export');
            Route::get('/{id}/activation','CategoryController@activation');
        });
        Route::group([
            'prefix'=>'features'
        ],function () {
            Route::get('/','FeatureController@index');
            Route::get('/create','FeatureController@create');
            Route::post('/','FeatureController@store');
            Route::get('/{id}','FeatureController@show');
            Route::get('/{id}/edit','FeatureController@edit');
            Route::put('/{id}','FeatureController@update');
            Route::delete('/{id}','FeatureController@destroy');
            Route::get('/option/export','FeatureController@export');
            Route::get('/{id}/activation','FeatureController@activation');
        });
        Route::group([
            'prefix'=>'utilities'
        ],function () {
            Route::get('/','UtilityController@index');
            Route::get('/create','UtilityController@create');
            Route::post('/','UtilityController@store');
            Route::get('/{id}','UtilityController@show');
            Route::get('/{id}/edit','UtilityController@edit');
            Route::put('/{id}','UtilityController@update');
            Route::delete('/{id}','UtilityController@destroy');
            Route::get('/option/export','UtilityController@export');
            Route::get('/{id}/activation','UtilityController@activation');
        });
        Route::group([
            'prefix'=>'countries'
        ],function () {
            Route::get('/','CountryController@index');
            Route::get('/create','CountryController@create');
            Route::post('/','CountryController@store');
            Route::get('/{id}','CountryController@show');
            Route::get('/{id}/edit','CountryController@edit');
            Route::put('/{id}','CountryController@update');
            Route::delete('/{id}','CountryController@destroy');
            Route::get('/option/export','CountryController@export');
            Route::get('/{id}/activation','CountryController@activation');
        });
        Route::group([
            'prefix'=>'cities'
        ],function () {
            Route::get('/','CityController@index');
            Route::get('/create','CityController@create');
            Route::post('/','CityController@store');
            Route::get('/{id}','CityController@show');
            Route::get('/{id}/edit','CityController@edit');
            Route::put('/{id}','CityController@update');
            Route::delete('/{id}','CityController@destroy');
            Route::get('/option/export','CityController@export');
            Route::get('/{id}/activation','CityController@activation');
        });
        Route::group([
            'prefix'=>'contacts'
        ],function () {
            Route::get('/','ContactController@index');
            Route::delete('/{id}','ContactController@destroy');
            Route::get('/option/export','ContactController@export');
        });
        Route::group([
            'prefix'=>'settings'
        ],function () {
            Route::get('/','SettingController@index');
            Route::get('/{id}/edit','SettingController@edit');
            Route::put('/{id}','SettingController@update');
        });
    });
    /*
    |--------------------------------------------------------------------------
    | Admin > App User
    |--------------------------------------------------------------------------
    | Here is where App User routes
    */

    Route::group([
        'prefix'=>'app_users',
        'namespace'=>'AppUser',
    ],function () {
        Route::group([
            'prefix'=>'users'
        ],function () {
            Route::get('/','UserController@index');
            Route::get('/create','UserController@create');
            Route::post('/','UserController@store');
            Route::get('/{user}','UserController@show');
            Route::get('/{user}/edit','UserController@edit');
            Route::put('/{user}','UserController@update');
            Route::delete('/{user}','UserController@destroy');
            Route::patch('/update/password',  'UserController@updatePassword');
            Route::get('/option/export','UserController@export');
            Route::get('/{id}/activation','UserController@activation');
        });
        Route::group([
            'prefix'=>'providers'
        ],function () {
            Route::get('/','ProviderController@index');
            Route::get('/create','ProviderController@create');
            Route::post('/','ProviderController@store');
            Route::get('/{user}','ProviderController@show');
            Route::get('/{user}/edit','ProviderController@edit');
            Route::put('/{user}','ProviderController@update');
            Route::delete('/{user}','ProviderController@destroy');
            Route::patch('/update/password',  'ProviderController@updatePassword');
            Route::get('/option/export','ProviderController@export');
            Route::get('/{id}/activation','ProviderController@activation');
        });
    });
    /*
    |--------------------------------------------------------------------------
    | Admin > App Data
    |--------------------------------------------------------------------------
    | Here is where App Data routes
    */

    Route::group([
        'prefix'=>'app_data',
        'namespace'=>'AppData',
    ],function () {
        Route::group([
            'prefix'=>'places'
        ],function () {
            Route::get('/','PlaceController@index');
            Route::get('/create','PlaceController@create');
            Route::post('/','PlaceController@store');
            Route::get('/{id}','PlaceController@show');
            Route::get('/{id}/edit','PlaceController@edit');
            Route::put('/{id}','PlaceController@update');
            Route::delete('/{id}','PlaceController@destroy');
            Route::get('/option/export','PlaceController@export');
            Route::get('/{id}/activation','PlaceController@activation');
        });
        Route::group([
            'prefix'=>'orders'
        ],function () {
            Route::get('/','OrderController@index');
            Route::get('/create','OrderController@create');
            Route::post('/','OrderController@store');
            Route::get('/{id}','OrderController@show');
            Route::get('/{id}/edit','OrderController@edit');
            Route::put('/{id}','OrderController@update');
            Route::delete('/{id}','OrderController@destroy');
            Route::get('/option/export','OrderController@export');
            Route::get('/{id}/activation','OrderController@activation');
        });
    });

});
